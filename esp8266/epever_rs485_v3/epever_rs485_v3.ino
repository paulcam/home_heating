#include <ModbusMaster.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#define MAX485_DE       D4
#define MAX485_RE       D1

#define PANEL_VOLTS     0x00
#define PANEL_AMPS      0x01
#define PANEL_POWER_L   0x02
#define PANEL_POWER_H   0x03
#define BATT_VOLTS      0x04
#define BATT_AMPS       0x05
#define BATT_POWER_L    0x06
#define BATT_POWER_H    0x07
#define LOAD_VOLTS      0x0C
#define LOAD_AMPS       0x0D
#define LOAD_POWER_L    0x0E
#define LOAD_POWER_H    0x0F

// LOW is on!
#define HB D4 

#define ERR_WIFI_CONNECT_FAILED 3
#define ERR_FAILED_READ 4
#define ERR_SUCCESS 1

#define FLASH_LO 250
#define FLASH_HI 250

// instantiate ModbusMaster object
ModbusMaster node;
const char* ssid = "*******";
const char* password = "******";

void flashErrorCode( int pulseCount ) {
  flash( pulseCount );
}

int connectWifi(boolean pulseIP) {
  WiFi.begin(ssid, password);
  WiFi.setAutoConnect(true);
  WiFi.setAutoReconnect(true);
  int wifiStatus = WiFi.status();
  // Rapid flash LED until connected.
  while (WiFi.status() != WL_CONNECTED)
  {
    digitalWrite(HB, LOW);
    delay(50);
    digitalWrite(HB, HIGH);
    delay(50);    
  }
  if( pulseIP ) {
    flashIpAddress();
  }
}

void flashIpAddress() {
    // Flash last octet of IP as three decimal digits
  // EG: 208 = Flash, flash, pause, pause, 8 flashes
  IPAddress ipaddress = WiFi.localIP();
  int lastOctect = ipaddress[3];
  int hundreds = lastOctect / 100;
  flash(hundreds);
  delay(500);
  int tens = (lastOctect - (hundreds*100)) / 10;
  flash(tens);
  delay(500);
  int units = (lastOctect - (hundreds*100) - (tens*10)) / 10;
  flash(units);
  delay(500);
}

void flash( int times ) {
  for( int i=0; i<times; i++) {
    digitalWrite(HB, LOW);
    delay(FLASH_LO);
    digitalWrite(HB, HIGH);
    delay(FLASH_HI);
  }
}


void setup()
{  
  pinMode(HB, OUTPUT);     // Initialize the heartbeat LED pin as an output

  
  connectWifi(true);

  // Setup the 485 control pins
  pinMode(MAX485_RE, OUTPUT);
  pinMode(MAX485_DE, OUTPUT);

  
  // Init in receive mode
  digitalWrite(MAX485_RE, 0);
  digitalWrite(MAX485_DE, 0);

  Serial.begin(115200);
  while (!Serial) {
    ;
  }

  // EPEver Device ID 1
  node.begin(1, Serial);

  // Callbacks 
  node.preTransmission(preTransmission);
  node.postTransmission(postTransmission);
}

void loop() {
  //connectWifi(false);
  readAndSendRealtimeData();
//  unsigned int sleep_secs = 5;
//  ESP.deepSleep(sleep_secs*1000000,WAKE_RF_DEFAULT);
  delay(5000);
}

void readAndSendRealtimeData()
{
  uint8_t result;

  do {
    // Read 16 registers starting at 0x3100)
    node.clearResponseBuffer();
    result = node.readInputRegisters(0x3100, 16);
    digitalWrite(HB,HIGH);
    if( result != node.ku8MBSuccess ) {
          flashErrorCode( ERR_FAILED_READ );
          delay(5000);
    }
  } while(result != node.ku8MBSuccess);

  float pV = node.getResponseBuffer(PANEL_VOLTS)/100.0f;
  float pI = node.getResponseBuffer(PANEL_AMPS)/100.0f;
  float pP = (node.getResponseBuffer(PANEL_POWER_L) |
                  (node.getResponseBuffer(PANEL_POWER_H) << 8))/100.0f;

  float bV = node.getResponseBuffer(BATT_VOLTS)/100.0f;
  float bI = node.getResponseBuffer(BATT_AMPS)/100.0f;
  float bP = (node.getResponseBuffer(BATT_POWER_L) |
                  (node.getResponseBuffer(BATT_POWER_H) << 8))/100.0f;

  float lV = node.getResponseBuffer(LOAD_VOLTS)/100.0f;
  float lI = node.getResponseBuffer(LOAD_AMPS)/100.0f;
  float lP = (node.getResponseBuffer(LOAD_POWER_L) |
                  (node.getResponseBuffer(LOAD_POWER_H) << 8))/100.0f;
                  
  WiFiUDP Udp;  
  Udp.beginPacket("10.0.0.3", 9999);

  char data[2048];
  memset(data, 0, 2048);
  //{"bedroom": {"key": "bedroom", "name": "Bedroom", "shortName": "BR", "value": "23.19", "type": "float", "units": "'C"}}

  //int len = snprintf(data, 1024, "pV:%.2f:V,pI:%.2f:A,pP:%.2f:W,bV:%.2f:V,bI:%.2f:A,bP:%.2f:W,lV:%.2f:V,lI:%.2f:A,lP:%.2f:W", pV, pI, pP, bV, bI, bP, lV, lI, lP);
  int len = snprintf(data, 2048, "{\"panelVolts\": {\"key\": \"panelVolts\", \"value\":%.2f,\"units\":\"V\",\"shortName\": \"pV\", \"type\":\"float\", \"name\": \"Panel Volts\"}, \"panelCurrent\": { \"key\": \"panelCurrent\", \"shortName\":\"pI\", \"value\":%.2f,\"units\":\"A\", \"type\":\"float\", \"name\": \"Panel Current\" }, \"panelPower\": { \"key\": \"panelPower\", \"shortName\":\"pP\", \"value\":%.2f,\"units\":\"W\", \"type\":\"float\", \"name\": \"Panel Power\" }, \"batteryVolts\": { \"key\": \"batteryVolts\", \"shortName\":\"bV\", \"value\":%.2f,\"units\":\"V\", \"type\":\"float\", \"name\": \"Battery Volts\" }, \"batteryCurrent\": { \"key\": \"batteryCurrent\", \"shortName\":\"bI\", \"value\":%.2f,\"units\":\"A\", \"type\":\"float\", \"name\": \"Battery Current\" }, \"batteryPower\": { \"key\": \"batteryPower\", \"shortName\":\"bP\", \"value\":%.2f,\"units\":\"W\", \"type\":\"float\", \"name\": \"Battery Power\" }, \"loadVolts\": { \"key\": \"loadVolts\", \"shortName\":\"lV\", \"value\":%.2f,\"units\":\"V\", \"type\":\"float\", \"name\": \"Load Volts\" }, \"loadCurrent\": { \"key\": \"loadCurrent\", \"shortName\":\"lI\", \"value\":%.2f,\"units\":\"A\", \"type\":\"float\", \"name\": \"Load Current\" }, \"loadPower\": { \"key\": \"loadPower\", \"shortName\":\"lP\", \"value\":%.2f,\"units\":\"W\", \"type\":\"float\", \"name\": \"Load Power\" }}", pV, pI, pP, bV, bI, bP, lV, lI, lP);
  Udp.write(data, len);
  Udp.endPacket();
  flashErrorCode( ERR_SUCCESS );
}

void preTransmission()
{
  digitalWrite(MAX485_RE, 1);
  digitalWrite(MAX485_DE, 1);
}

void postTransmission()
{
  digitalWrite(MAX485_RE, 0);
  digitalWrite(MAX485_DE, 0);
}
