#include "credentials.h"
#include "MyESPDevice.hpp"
#include <ESP8266WebServer.h>
#include "Button.hpp"

#define FIRMWARE BUILD_FIRMWARE
#define SENSOR_NAME BUILD_SENSORNAME
#define SENSOR_ZONE BUILD_SENSORZONE

const char* ssid = STASSID;
const char* password = STAPSK;
const int ledPin = 13;
const int relayPin = 12;
const int buttonPin = 0;
int relay = LOW;

ESP8266WebServer server(80);
Button button(buttonPin, Serial);
WiFiUDP udp;

void sendStatus(int status) {
  char json[512];
  memset(json, 0, 512);

  int len = snprintf((char*)json, 511, "{\"radiator%s\": {\"key\": \"radiator%s\", \"name\": \"%s\", \"shortName\": \"XX\", \"value\": %d, \"type\": \"bool\", \"units\": \"None\"}}", SENSOR_ZONE, SENSOR_ZONE, SENSOR_NAME,status );
  Serial.print("Sending UDP packet:");
  Serial.println(json);
  // Prepare UDP sender
  udp.beginPacket("10.0.0.3", 9999);
  udp.write(json, len);
  udp.endPacket();  
}

/*
 * HTTP Handlers
 */
void handleOn() {
  relay = HIGH;
  digitalWrite(relayPin, HIGH);
  server.send(200, "application/json", "{\"status\": \"ON\"}");
  // Flash LED Once
  digitalWrite(ledPin, LOW);
  delay(100);
  digitalWrite(ledPin, HIGH);

  sendStatus(1);
}

void handleOff() {
  relay = LOW;
  digitalWrite(relayPin, LOW);
  server.send(200, "application/json", "{\"status\": \"OFF\"}");
  // Flash LED Once
  digitalWrite(ledPin, LOW);
  delay(100);
  digitalWrite(ledPin, HIGH);
  
  sendStatus(0);

}

void handleStatus() {
  if( relay == HIGH ) {
    server.send(200, "application/json", "{\"status\": \"ON\"}");
  } else {
    server.send(200, "application/json", "{\"status\": \"OFF\"}");
  }
}

/**
 * Button handler functions
 */
void handleClick(unsigned int pin) {
  Serial.print("Click on pin: ");
  Serial.println(pin);
  if(pin == buttonPin) {
    digitalWrite(relayPin, !digitalRead(relayPin));
    relay = digitalRead(relayPin);
    sendStatus(relay);
  }
  // Flash LED once
  digitalWrite(ledPin, LOW);
  delay(100);
  digitalWrite(ledPin, HIGH);

}

void handleLongClick(unsigned int pin) {
  Serial.print("Long click on pin: ");
  Serial.println(pin);
  bool state;
  state = (relay == LOW);
  
  udp.beginPacket("10.0.0.3", 9997);

  char data[2048];
  memset(data, 0, 2048);  
  int len = snprintf(data, 2048, "{\"heating-%s\": {\"key\": \"heating-%s\", \"category\": \"HEATING\", \"demandState\":\"%s\", \"expiry\":\"3600\"}}", SENSOR_ZONE, SENSOR_ZONE, state ? "ON" : "OFF");
  Serial.print("Sending UDP packet:");
  Serial.println(data);
  udp.write(data, len);
  udp.endPacket();
  // Flash LED twice
  for(int i=0; i<2; i++) {
    digitalWrite(ledPin, LOW);
    delay(100);
    digitalWrite(ledPin, HIGH);
    delay(150);
  }
}

void setup() {
  if(! MyESPDevice.setup(SENSOR_NAME, FIRMWARE, ssid, password ) ) {
    ESP.restart();
  }
  IPAddress ip(10,0,0,22);   
  IPAddress gateway(10,0,0,1);   
  IPAddress subnet(255,255,255,0); 
  WiFi.config(ip, gateway, subnet);

  uint32_t bcastInt = MyESPDevice.getBroadcast();
  IPAddress bcast(bcastInt);
  Serial.println("Broadcast : "+bcast.toString());  
  Serial.println();
  Serial.println(FIRMWARE);

  pinMode(ledPin, OUTPUT);
  pinMode(relayPin, OUTPUT);
  digitalWrite(relayPin, LOW);

  
  button.onClick(handleClick);
  button.onLongClick(handleLongClick);

  server.on("/on", handleOn);
  server.on("/off", handleOff);
  server.on("/status", handleStatus);  
  server.begin();
}
void loop() {
  MyESPDevice.handle();

  server.handleClient();
  button.update();
  delay(3);
}