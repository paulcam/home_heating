#include <OneWire.h>
#include <ESP8266WiFi.h>
#include <DallasTemperature.h>
#include <WiFiUdp.h>

// Put your wifi SID and password in a file of this name
// With content
// const char* ssid = "ssid";
// const char* password = "password";
// OR comment out the below inlclude and uncomment/edit the above consts
// #include "credentials.h"
#define ONE_WIRE_BUS D2
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
const int ledPin = LED_BUILTIN;

void flash(int pin, int repeats, int delayMillis) {
  for( int i=0; i<repeats*2; i++) {
    digitalWrite( pin, !digitalRead(pin) );
    delay( delayMillis);
  }
}

void setup() {
  // Set pin modes
  pinMode(D2, INPUT);
  pinMode(ledPin, OUTPUT);

  // Open serial for debug
  Serial.begin(9600);
  
  Serial.print("Connecting to WIFI...");
  WiFi.begin(ssid, password);
  
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    flash(ledPin, 1, 50);
    Serial.print(".");
  }

  // CHANGE YOUR IP ETC. HERE IF YOU WANT A STATIC ONE
  // Set IP, subnet and gateway (can be commented out for DHCP)
  //IPAddress ip(10,0,0,46);   
  //IPAddress gateway(10,0,0,1);   
  //IPAddress subnet(255,255,255,0);  
  //WiFi.config(ip, gateway, subnet);
  Serial.println("DONE");

  // Start up 1 wire bus
  sensors.begin();
}


// If you intend to change how the data is sent, do it here.  Most of the code above is setup and config.
void loop() {
  // Sample temperatures.
  Serial.print(" Requesting temperatures...");
  sensors.requestTemperatures(); // Send the command to get temperatures
  Serial.println("DONE");

  // Get device 1 temp and debug print
  Serial.print("Temperature for Device 1 is: ");
  double temp = sensors.getTempCByIndex(0);
  Serial.println(temp); 

  // Prepare UDP sender
  WiFiUDP udp;  
  
  // Send device 1 temp JSON
  uint8_t data[255];
  memset(data, 0, 255);
  int len = snprintf((char*)data, 255, "{\"test1\": {\"key\": \"test1\", \"name\": \"Heating In Pipe\", \"shortName\": \"T1\", \"value\": \"%.2f\", \"type\": \"float\", \"units\": \"'C\"}}", temp);
  udp.beginPacket("10.0.0.3", 9999);
  udp.write(data, len);
  udp.endPacket();

  // Get device 2 temp and debug print
  Serial.print("Temperature for Device 2 is: ");
  temp = sensors.getTempCByIndex(1);
  Serial.println(temp); 

  // Send device 2 temp JSON
  memset(data, 0, 255);
  len = snprintf((char*)data, 255, "{\"test2\": {\"key\": \"test2\", \"name\": \"Heating Out Pipe\", \"shortName\": \"T2\", \"value\": \"%.2f\", \"type\": \"float\", \"units\": \"'C\"}}", temp);
  udp.beginPacket("10.0.0.3", 9999);
  udp.write(data, len);
  udp.endPacket();
  
  delay(5000);
}
