#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include "Button.hpp"
#include <Pinger.h>
#include <Ticker.h>

const char* ssid = "xxx";
const char* password = "xxx";
const int ledPin = 13;
const int relayPin = 12;
const int buttonPin = 0;
int relay = LOW;
const char compile_date[] = __DATE__ " " __TIME__;

ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;
Button button(buttonPin, Serial);
Pinger pinger;
Ticker timer;

/*
 * HTTP Handlers
 */
void handleOn() {
  relay = HIGH;
  digitalWrite(relayPin, HIGH);
  server.send(200, "application/json", "{\"status\": \"ON\"}");
  // Flash LED Once
  digitalWrite(ledPin, LOW);
  delay(100);
  digitalWrite(ledPin, HIGH);
}

void handleOff() {
  relay = LOW;
  digitalWrite(relayPin, LOW);
  server.send(200, "application/json", "{\"status\": \"OFF\"}");
  // Flash LED Once
  digitalWrite(ledPin, LOW);
  delay(100);
  digitalWrite(ledPin, HIGH);
}

void handleStatus() {
  if( relay == HIGH ) {
    server.send(200, "application/json", "{\"status\": \"ON\"}");
  } else {
    server.send(200, "application/json", "{\"status\": \"OFF\"}");
  }
}

void handlePing() {
  if (server.arg("ip")== ""){
    Serial.println("No IP provided to ping");
    server.send(400, "text/plain", "Try /ping?ip=1.2.3.4");

    return;
  }  
  Serial.print("Pinging ");  
  Serial.print(server.arg("ip"));
  
 if(pinger.Ping(server.arg("ip"))){
    Serial.println("- Success");
    server.send(200, "text/plain", "SUCCESS");
  } else {
    Serial.println("- Failed");
    server.send(200, "text/plain", "FAILURE");
  }
}

void selfPing() {
  pinger.Ping("10.0.0.72");
  pinger.Ping("10.0.0.3"); // Hub
  pinger.Ping("10.0.0.199"); // Server
}

/**
 * Button handler functions
 */
void handleClick(int pin) {
  Serial.print("Click on pin: ");
  Serial.println(pin);
  if(pin == buttonPin) {
    digitalWrite(relayPin, !digitalRead(relayPin));
    relay = digitalRead(relayPin);
  }
  // Flash LED once
  digitalWrite(ledPin, LOW);
  delay(100);
  digitalWrite(ledPin, HIGH);

}

void handleLongClick(int pin) {
  Serial.print("Long click on pin: ");
  Serial.println(pin);
  bool state;
  state = (relay == LOW);
  
  WiFiUDP Udp;  
  Udp.beginPacket("10.0.0.3", 9997);

  char data[2048];
  memset(data, 0, 2048);  
  int len = snprintf(data, 2048, "{\"groupLight1\": {\"key\": \"groupLight1\", \"demandState\":\"%s\", \"expiry\":\"60\"}}", state ? "ON" : "OFF");
  Serial.print("Sending UDP packet:");
  Serial.println(data);
  Udp.write(data, len);
  Udp.endPacket();
  // Flash LED twice
  for(int i=0; i<2; i++) {
    digitalWrite(ledPin, LOW);
    delay(100);
    digitalWrite(ledPin, HIGH);
    delay(150);
  }
}

/**
 * SETUP
 */
void setup() {
  
  pinMode(ledPin, OUTPUT);
  pinMode(relayPin, OUTPUT);
  digitalWrite(relayPin, LOW);
  Serial.begin(115200);
  IPAddress ip(10,0,0,72);   
  IPAddress gateway(10,0,0,1);   
  IPAddress subnet(255,255,255,0);   

  WiFi.mode(WIFI_STA);
  WiFi.setSleepMode(WIFI_NONE_SLEEP);
  WiFi.begin(ssid, password);

  Serial.print("Compile timestamp: ");
  Serial.println(compile_date);

  Serial.print("Connecting to WiFi");

  button.onClick(handleClick);
  button.onLongClick(handleLongClick);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(ledPin, LOW);
    delay(50);
    digitalWrite(ledPin, HIGH);
    delay(50);
    Serial.print(".");
    button.update();
  }
  WiFi.config(ip, gateway, subnet);

  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  
  server.on("/on", handleOn);
  server.on("/off", handleOff);
  server.on("/status", handleStatus);
  server.on("/ping", handlePing);
  httpUpdater.setup(&server);
  timer.attach(10, selfPing);
  server.begin();
}

void loop() {
  server.handleClient();
  button.update();
  delay(3);

}
