#include <OneWire.h>
#include <ESP8266WiFi.h>
#include <DallasTemperature.h>
#include <WiFiUdp.h>

// Put your wifi SID and password in a file of this name
// With content
// const char* ssid = "mysid";
// const char* password = "mypassword";
#include "credentials.h"
#define ONE_WIRE_BUS D1
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);


void setup() {
  Serial.begin(9600);
  Serial.println("Dallas Temperature IC Control Library Demo");
  Serial.print("Connecting to WIFI...");
  WiFi.begin(ssid, password);
  Serial.println("DONE");
  sensors.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.print(" Requesting temperatures...");
  sensors.requestTemperatures(); // Send the command to get temperatures
  Serial.println("DONE");

  Serial.print("Temperature for Device 1 is: ");
  double temp = sensors.getTempCByIndex(0);
  Serial.print(temp); 


  
  WiFiUDP udp;  
  udp.beginPacket("10.0.0.3", 9999);

  uint8_t data[255];
  memset(data, 0, 255);
  int len = snprintf((char*)data, 255, "{\"heatingInPipe\": {\"key\": \"heatingInPipe\", \"name\": \"Heating In Pipe\", \"shortName\": \"HI\", \"value\": \"%.2f\", \"type\": \"float\", \"units\": \"'C\"}}", temp);
  udp.beginPacket("10.0.0.3", 9999);
  udp.write(data, len);
  udp.endPacket();

  Serial.print("Temperature for Device 2 is: ");
  temp = sensors.getTempCByIndex(1);
  Serial.print(temp); 

  memset(data, 0, 255);
  len = snprintf((char*)data, 255, "{\"heatingOutPipe\": {\"key\": \"heatingOutPipe\", \"name\": \"Heating Out Pipe\", \"shortName\": \"HO\", \"value\": \"%.2f\", \"type\": \"float\", \"units\": \"'C\"}}", temp);
  udp.beginPacket("10.0.0.3", 9999);
  udp.write(data, len);
  udp.endPacket();
  
  delay(5000);
}
