#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include "Button.hpp"
#include "HeatingController.hpp"
#include <Pinger.h>
#include <Ticker.h>

const char* ssid = "xxx";
const char* password = "xxx";
const int ledPin = 13;
const int relayPin = 12;
const int buttonPin = 0;
int relay = LOW;
const char compile_date[] = __DATE__ " " __TIME__;
long onTime = 0;
long offTime = 0;

ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;
Button button(buttonPin, Serial);
HeatingController heatingController( relayPin, ledPin );

Pinger pinger;
Ticker timer;

/**
 * Timer handlers
 */
void ping() {
  if(pinger.Ping("10.0.0.9")){
    Serial.println("- Success");
    server.send(200, "text/plain", "SUCCESS");
  } else {
    Serial.println("- Failed");
    server.send(200, "text/plain", "FAILURE");
  }
}
/*
 * HTTP Handlers
 */
void handleOn() {
  if(! heatingController.switchOn() ) {
    flash(ledPin, 5, 100);
    server.send(400, "application/json", "{\"error\": \"Could not turn heating on.\"}");
    return;
  }
  server.send(200, "application/json", "{\"status\": \"ON\"}");
  // Flash LED Once
  flash(ledPin, 1, 100);
}

void handleOff() {
  if(! heatingController.switchOff() ) {
    flash(ledPin, 5, 100);
    server.send(400, "application/json", "{\"error\": \"Could not turn heating off.\"}");
    return;
  }
  server.send(200, "application/json", "{\"status\": \"OFF\"}");
  // Flash LED Once
  flash(ledPin, 1, 100);
}

void handleStatus() {
  if( heatingController.getStatus() == HIGH ) {
    server.send(200, "application/json", "{\"status\": \"ON\"}");
  } else {
    server.send(200, "application/json", "{\"status\": \"OFF\"}");
  }
}

void handlePing() {
  if (server.arg("ip")== ""){
    Serial.println("No IP provided to ping");
    server.send(400, "text/plain", "Try /ping?ip=1.2.3.4");

    return;
  }  
  Serial.print("Pinging ");  
  Serial.print(server.arg("ip"));
  
 if(pinger.Ping(server.arg("ip"))){
    Serial.println("- Success");
    server.send(200, "text/plain", "SUCCESS");
  } else {
    Serial.println("- Failed");
    server.send(200, "text/plain", "FAILURE");
  }
}

/**
 * Button handler functions
 */
void handleClick(int pin) {
  Serial.print("Click on pin: ");
  Serial.println(pin);
  if(pin == buttonPin) {
    if( ! heatingController.toggle() ) {
      flash(ledPin, 5, 100);
      return;
    }
    flash(ledPin, 1, 100);
  }
}

void flash(int pin, int repeats, int delayMillis) {
  for( int i=0; i<repeats*2; i++) {
    digitalWrite( pin, !digitalRead(pin) );
    delay( delayMillis);
  }
}

void handleLongClick(int pin) {  
  Serial.print("Long click on pin: ");
  Serial.println(pin);
  if( pin != buttonPin ) {
    return;
  }
  int state;
  state = heatingController.getStatus();
  
  WiFiUDP Udp;  
  Udp.beginPacket("10.0.0.3", 9997);

  char data[2048];
  memset(data, 0, 2048);  
  int len = snprintf(data, 2048, "{\"heating\": {\"key\": \"heating\", \"demandState\":\"%s\", \"expiry\": \"3600\"}}", (state==HIGH) ? "OFF" : "ON");
  Serial.print("Sending UDP packet:");
  Serial.println(data);
  Udp.write(data, len);
  Udp.endPacket();
  // Flash LED twice
  flash( ledPin, 2, 100);
}

/**
 * SETUP
 */
void setup() {
  
  pinMode(ledPin, OUTPUT);
  pinMode(relayPin, OUTPUT);
  digitalWrite(ledPin, HIGH); // LED off.
  Serial.begin(115200);
  
  // Default to ON!
  if( ! heatingController.switchOn() ) {
    Serial.println("Failed to turn heating on at boot up!");
    flash( ledPin, 5, 100);
  }
    
  IPAddress ip(10,0,0,9);   
  IPAddress gateway(10,0,0,1);   
  IPAddress subnet(255,255,255,0);   

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  Serial.print("Compile timestamp: ");
  Serial.println(compile_date);

  Serial.print("Connecting to WiFi");

  button.onClick(handleClick);
  button.onLongClick(handleLongClick);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    flash(ledPin, 1, 50);
    Serial.print(".");
    button.update();
  }
  WiFi.config(ip, gateway, subnet);

  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  
  server.on("/on", handleOn);
  server.on("/off", handleOff);
  server.on("/status", handleStatus);
  server.on("/ping", handlePing);
  httpUpdater.setup(&server);
  timer.attach(60, ping);
  server.begin();
}

void loop() {
  server.handleClient();
  button.update();
  delay(3);

}
