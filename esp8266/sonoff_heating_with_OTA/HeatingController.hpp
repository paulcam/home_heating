const unsigned long MINIMUM_ON_TIME = 10000;
const unsigned long MINIMUM_OFF_TIME = 60000;

class HeatingController {
private: 
  unsigned long onTime;
  unsigned long offTime;
  int relayPin;
  int ledPin;
  
public:
  HeatingController(int relayPinPrm, int ledPinPrm) {
    relayPin = relayPinPrm;    
    ledPin = ledPinPrm;
    offTime=0;
    onTime=0;
  }

  boolean switchOn() {
    unsigned long currentMillis = millis();
    Serial.printf("switchOn currentMillis %d offTime %d\n", currentMillis, offTime );
    // Minimum OFF time.
    // If off time was greater than 1 minute ago, or the millis have clock round.
    if( offTime==0 || (currentMillis - offTime < 0) || (currentMillis - offTime > MINIMUM_OFF_TIME) ) {
      digitalWrite(relayPin, HIGH);
      digitalWrite(ledPin, LOW); // LED is inverse
      onTime = millis();
      return true;
    }
    Serial.println("Minimum off time not passed");
    return false;
  }
  boolean switchOff() {
    unsigned long currentMillis = millis();
    Serial.printf("switchOff currentMillis %d onTime %d\n", currentMillis, onTime );
    // Minimum ON time.
    // If on time was greater than 1 minute ago, or the millis have clock round.
    if( onTime==0 || (currentMillis - onTime < 0) || (currentMillis - onTime > MINIMUM_ON_TIME) ) {
      digitalWrite(relayPin, LOW);
      digitalWrite(ledPin, HIGH); // LED is inverse
      offTime = millis();
      return true;
    }
    Serial.println("Minimum on time not passed");
    return false;
  }
  boolean toggle() {
    int status = getStatus();
    if( status == HIGH ) {
      return switchOff();
    } else {
      return switchOn();
    }
  }
  int getStatus() {
    return digitalRead(relayPin);
  }
};
