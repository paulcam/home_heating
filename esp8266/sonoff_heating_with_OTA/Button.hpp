class Button {
  private: 
    int buttonPin;
    HardwareSerial &serial;

    int buttonState = HIGH;

    void (*buttonDownFunc)(int) = NULL;
    void (*buttonUpFunc)(int) = NULL;
    void (*clickFunc)(int) = NULL;
    void (*longClickFunc)(int) = NULL;

    long longClickDelay = 2000;
    long shortClickDelay = 50;
    unsigned long buttonDownTime = millis();
    
  public:
    Button(int bp, HardwareSerial &rs) : serial(rs){
      buttonPin = bp;      
      digitalWrite(buttonPin, LOW);
      pinMode(buttonPin, INPUT);
    }
    
    void update(){
      int currentState = digitalRead(buttonPin);

      if( buttonState != currentState ) {
        if( buttonState == HIGH ) {
          serial.println("Button DOWN!");
          if(buttonDownFunc != NULL) {
            buttonDownFunc(buttonPin);            
          }
          buttonDownTime = millis();
        } else {
          serial.println("Button UP!");
          if(buttonUpFunc != NULL) {
            buttonUpFunc(buttonPin);
          }
          if(millis() - buttonDownTime > longClickDelay) {
            if(longClickFunc!=NULL) {
              longClickFunc(buttonPin);
            }
          } else if (millis() - buttonDownTime > shortClickDelay) {
            if( clickFunc != NULL ) {
              clickFunc(buttonPin);
            }
          }
        }
        buttonState = currentState;
      }
    }

    void onButtonDown(void (*buttonDown)(int)) {
      buttonDownFunc = buttonDown;
    }

    void onButtonUp(void (*buttonUp)(int)) {
      buttonUpFunc = buttonUp;
    }

    void onClick(void (*clickF)(int)) {
      clickFunc = clickF;
    }
    
    void onLongClick(void (*longClick)(int)) {
      longClickFunc = longClick;
    }
};
