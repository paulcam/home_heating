#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

class MyESPDeviceClass
{
private:
public:
    static String firmware;

    static uint32_t getBroadcast() {
        IPAddress ip = WiFi.localIP();
        IPAddress mask = WiFi.subnetMask();
        uint32_t ipInt = ip.v4();
        uint32_t maskInt = mask.v4();
        uint32_t bcastInt = ipInt | ( ~ maskInt );
        
        return bcastInt;
    }

    static void broadcastPresence()
    {
        uint32_t bcast = getBroadcast();
        IPAddress bcastIp = IPAddress(bcast);
        String bcastStr = bcastIp.toString();
        String macAddress = WiFi.macAddress();
        String ipAddress = WiFi.localIP().toString();

        char data[2048];
        memset(data, 0, 2048);
        int len = snprintf(data, 2048, "{\"macAddress\": \"%s\", \"firmware\": \"%s\", \"ipAddress\": \"%s\"}", 
            macAddress.c_str(), MyESPDeviceClass::firmware.c_str(), ipAddress.c_str() );

        Serial.println(data);
        WiFiUDP Udp;
        noInterrupts();
        Udp.beginPacket(bcastStr.c_str(), 8267);

        Udp.write(data, len);
        Udp.endPacket();
        interrupts();
    }

    static bool setup(const String hostname, const char *firmware, const char *ssid, const char *password)
    {
        MyESPDeviceClass::firmware = firmware; 
        Serial.begin(115200);
        Serial.println("MyESPDevice Booting as " + hostname + " with firmware "+MyESPDeviceClass::firmware);
        WiFi.mode(WIFI_STA);
        WiFi.begin(ssid, password);
        pinMode(LED_BUILTIN, OUTPUT);
        while(!WiFi.isConnected() && WiFi.status() != WL_CONNECT_FAILED) {
            digitalWrite(LED_BUILTIN, HIGH);
            delay(100);
            digitalWrite(LED_BUILTIN, LOW);
            delay(100);
        }
        if (WiFi.waitForConnectResult() != WL_CONNECTED)
        {
            Serial.println("Connection Failed!");
            return false;
        }
        ArduinoOTA.setHostname(hostname.c_str());
        WiFi.hostname(hostname.c_str());

        ArduinoOTA.onStart([]() {
            String type;
            if (ArduinoOTA.getCommand() == U_FLASH)
            {
                type = "sketch";
            }
            else
            { // U_SPIFFS
                type = "filesystem";
            }

            // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
            Serial.println("Start updating " + type);
        });
        ArduinoOTA.onEnd([]() {
            Serial.println("\nEnd");
        });
        ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
            Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
        });
        ArduinoOTA.onError([](ota_error_t error) {
            Serial.printf("Error[%u]: ", error);
            if (error == OTA_AUTH_ERROR)
            {
                Serial.println("Auth Failed");
            }
            else if (error == OTA_BEGIN_ERROR)
            {
                Serial.println("Begin Failed");
            }
            else if (error == OTA_CONNECT_ERROR)
            {
                Serial.println("Connect Failed");
            }
            else if (error == OTA_RECEIVE_ERROR)
            {
                Serial.println("Receive Failed");
            }
            else if (error == OTA_END_ERROR)
            {
                Serial.println("End Failed");
            }
        });
        ArduinoOTA.begin();
        Serial.println("Ready");
        Serial.print("IP address: ");
        Serial.println(WiFi.localIP());
  
        MyESPDeviceClass::broadcastPresence();
  
        return true;
    }

    static void handle()
    {
        static int last = millis();
        ArduinoOTA.handle();
        if( millis() - last > 10000 ) {
            last = millis();
            MyESPDeviceClass::broadcastPresence();
        }
    }
};
String MyESPDeviceClass::firmware{ String("") };
MyESPDeviceClass MyESPDevice;