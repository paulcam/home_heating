#include "credentials.h"
#include "MyESPDevice.hpp"
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define FIRMWARE BUILD_FIRMWARE
#define SENSOR_NAME BUILD_SENSORNAME
#define SENSOR_ZONE BUILD_SENSORZONE

const char* ssid = STASSID;
const char* password = STAPSK;
const uint ledPin = LED_BUILTIN;
Adafruit_BME280 bme;
void setup() {
  if(! MyESPDevice.setup(SENSOR_NAME, FIRMWARE, ssid, password ) ) {
    ESP.restart();
  }
  uint32_t bcastInt = MyESPDevice.getBroadcast();
  IPAddress bcast(bcastInt);
  Serial.println("Broadcast : "+bcast.toString());  
  bme.begin(0x76);   
  Serial.println();
  Serial.println(FIRMWARE);
}
void loop() {
  MyESPDevice.handle();
  float temperature = bme.readTemperature();
  float humidity = bme.readHumidity();
  float pressure = bme.readPressure() / 100.0F;
  
  Serial.printf("Temp: %.2f  Hum: %.2f, Pressure: %.2f",temperature,humidity,pressure);
  
  char json[512];
  memset(json, 0, 512);

  int len = snprintf((char*)json, 511, "{\"%s\": {\"key\": \"%s\", \"name\": \"%s\", \"shortName\": \"XX\", \"value\": \"%.2f\", \"type\": \"float\", \"units\": \"'C\"}}", SENSOR_ZONE, SENSOR_ZONE, SENSOR_NAME, temperature);
  // Prepare UDP sender
  WiFiUDP udp;
  udp.beginPacket("10.0.0.3", 9999);
  udp.write(json, len);
  udp.endPacket();

  len = snprintf((char*)json, 511, "{\"%sHumidity\": {\"key\": \"%sHumidity\", \"name\": \"%s humidity\", \"shortName\": \"XX\", \"value\": \"%.2f\", \"type\": \"float\", \"units\": \"\%\"}}", SENSOR_ZONE, SENSOR_ZONE, SENSOR_NAME, humidity);
  udp.beginPacket("10.0.0.3", 9999);
  udp.write(json, len);
  udp.endPacket();

  delay(5000);
}