#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include "Button.hpp"

const char* ssid = "xxx";
const char* password = "xxx";
const int ledPin = 13;
const int relayPin = 12;
const int buttonPin = 0;
int relay = LOW;
const char compile_date[] = __DATE__ " " __TIME__;

ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;
Button button(buttonPin, Serial);

void handleOn() {
  relay = HIGH;
  digitalWrite(relayPin, HIGH);
  digitalWrite(ledPin, LOW);

  server.send(200, "application/json", "{\"status\": \"ON\"}");
}

void handleOff() {
  relay = LOW;
  digitalWrite(relayPin, LOW);
  digitalWrite(ledPin, HIGH);
  server.send(200, "application/json", "{\"status\": \"OFF\"}");
}

void handleStatus() {
  if( relay == HIGH ) {
    server.send(200, "application/json", "{\"status\": \"ON\"}");
  } else {
    server.send(200, "application/json", "{\"status\": \"OFF\"}");
  }
}

void handleClick(int pin) {
  Serial.print("Click on pin: ");
  Serial.println(pin);
  if(pin == buttonPin) {
    digitalWrite(relayPin, !digitalRead(relayPin));
    digitalWrite(ledPin, !digitalRead(ledPin));
    relay = digitalRead(relayPin);
  }
}

void handleLongClick(int pin) {
  Serial.print("Long click on pin: ");
  Serial.println(pin);
  bool state;
  state = (relay == LOW);
  
  WiFiUDP Udp;  
  Udp.beginPacket("10.0.0.5", 9997);

  char data[2048];
  memset(data, 0, 2048);  
  int len = snprintf(data, 2048, "{\"groupLight1\": {\"key\": \"groupLight1\", \"demandState\":\"%s\"}}", state ? "ON" : "OFF");
  Serial.print("Sending UDP packet:");
  Serial.println(data);
  Udp.write(data, len);
  Udp.endPacket();
}

void setup() {
  
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
  pinMode(relayPin, OUTPUT);
  for(int i=0; i<5; i++) {
    digitalWrite(ledPin, LOW);
    delay(500);
    digitalWrite(ledPin, HIGH);
    delay(500);
  }
  digitalWrite(relayPin, LOW);
  Serial.begin(115200);
  IPAddress ip(10,0,0,7);   
  IPAddress gateway(10,0,0,1);   
  IPAddress subnet(255,255,255,0);   

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  Serial.print("Compile timestamp: ");
  Serial.println(compile_date);

  Serial.print("Connecting to WiFi");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  WiFi.config(ip, gateway, subnet);

  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  button.onClick(handleClick);
  button.onLongClick(handleLongClick);
  
  server.on("/on", handleOn);
  server.on("/off", handleOff);
  server.on("/status", handleStatus);
  httpUpdater.setup(&server);
  server.begin();
  Serial.println("HTTP server started");
}

void loop() {
  server.handleClient();
  button.update();
  delay(3);

}
