import datetime
import logging
import json
from libs.logging_setup import setup_logging
from libs.master_clock import MasterClock
from libs.mqtt_lib import MqttClient, MqttMultiTopicObservableCache, MqttTopicMatcher
from libs.time_conditions import Condition, TimeCondition, DayOfWeekCondition
from libs.value_objects import Request, Demand
import time

setup_logging()

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)

class BaseSchedule:
    def __init__(self):
        self.conditions = []

    def is_active(self):
        clock = self.get_clock()
        data = { "currentTime": clock }
        state = True
        for condition_tuple in self.conditions:
            modifier = condition_tuple[0]
            condition = condition_tuple[1]

            # print("Modifier: {0!s} Condition: {1!s}".format(modifier, condition))
            if modifier == "OR":
                state = Condition._or(state, condition, data)
                # print( " OR {0!s}({1!s})".format(condition, state))
            elif modifier == "AND":
                state = Condition._and(state, condition, data)
                # print(" AND {0!s}({1!s})".format(condition, state))
            else:
                print("Unknown modifier: {0!s}".format(modifier))
        return state

    def get_clock(self):
        return MasterClock.instance.get_current_time()


class ZoneProfileSchedule(BaseSchedule):
    def __init__(self, name, conditions, cache, profile,
                 data_topic_root="home/sensors/temperature",
                 request_topic_root="home/heating/requests",
                 max_age=300):
        super().__init__()
        self.conditions = conditions
        self.logger = module_logger.getChild(self.__class__.__name__)
        self.logger.addHandler(logging.NullHandler())

        self.name = name
        self.cache = cache
        self.request_topic_root = request_topic_root
        self.profile = profile
        self.data_topic_root = data_topic_root
        self.max_age = max_age

        # Subscribe to and monitor our profile
        self.cache.add_observer(profile+"/+", self)
        self.cache.add_subscription(profile+"/+")
        # observers will be added dynamically
        self.cache.add_subscription(data_topic_root + "/+")
        self.logger.info("Creating zone target schedule for topic {0!s} and profile {1!s}"
                         .format(request_topic_root, profile))

    def notify(self, topic):
        self.logger.debug("Notified of topic {0!s}".format(topic))
        # is sensor or profile?
        if MqttTopicMatcher.matches(topic, self.profile+"/+"):
            # Profile update
            # New zone?
            zone = str(topic).rpartition("/")[2]
            self.cache.add_observer(self.data_topic_root + "/" + zone, self)
            self.evaluate(zone)
        elif MqttTopicMatcher.matches(topic, self.data_topic_root + "/+"):
            # Sensor update
            zone = str(topic).rpartition("/")[2]
            self.logger.debug("Data update for zone {0!s}".format(zone))
            self.evaluate(zone)

    def evaluate(self, zone):
        clock = self.get_clock()
        sch_data = {
            "name": self.name,
            "active": False,
            "targetProfile": self.profile,
            "timestamp": clock,
            "hr_time": time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(clock))
        }
        if not self.is_active():
            self.logger.debug("Schedule {0!s} inactive.".format(self.name))
            cache.client.publish_retained("home/heating/schedules/{0!s}".format(self.name), json.dumps(sch_data) )
            return False
        sch_data["active"] = True

        self.logger.debug("Schedule {0!s} active.".format(self.name))
        cache.client.publish_retained("home/heating/schedules/{0!s}".format(self.name), json.dumps(sch_data))

        if not zone:
            return False
        current = self.cache.last_for_topic(self.data_topic_root + "/" + zone)
        if not current:
            return False
        if "timestamp" in current:
            timestamp = float(current["timestamp"])
            if timestamp + self.max_age < clock:
                self.logger.debug("Expired data ignored Now: {1!s} Then: {2!s} = {3!s} - {0!s}".format(current, clock, timestamp, clock-timestamp))
                return False
        else:
            self.logger.warning("WARN: Permitting data without timestamp {0!s}".format(current))

        target = self.cache.last_for_topic(self.profile+"/"+zone)

        self.logger.debug("Current {0!s} - Target: {1!s}".format(current, target))
        current = float(current["value"])
        request_data = self.evaluate_data( target, current, zone, clock)
        if request_data:
            request = Request(request_data, zone)
            self.logger.info("Zone {0!s} target {1!s} not met with {2!s} requesting heating for {3!s} seconds"
                             .format(zone, target, current, 60))
            self.publish_request(request)

    def evaluate_data(self, target, current, zone, clock):
        cache.client.publish_retained("home/heating/active_targets/{0!s}".format(zone), json.dumps({
            "target":target["value"],
            "timestamp": clock,
            "profile": self.profile or "None",
            "hr_time": time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(clock)),
            "schedule": self.name,
            "sympathetic": False }))

        if current < float(target["value"]):
            return {
                "timestamp": clock,
                "hr_time": time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(clock)),
                "state": "ON",
                "zone": zone,
                "expiry": 60,
                "metadata": {
                    "schedule": {
                        "name": self.name,
                        "targetProfile": self.profile
                    },
                    "target": float(target["value"]),
                    "current": current
                }
            }
        return None

    def publish_request(self, request):
        request_json = request.to_json()
        topic = self.request_topic_root+"/"+request.data["zone"]
        self.cache.client.publish_retained(topic, request_json)


class PresenceZoneProfileSchedule(ZoneProfileSchedule):
    def __init__(self, name, conditions, cache, profile,
                 data_topic_root="home/sensors/temperature",
                 request_topic_root="home/heating/requests",
                 max_age=300,
                 presence_topic_root="home/presence"):
        super().__init__(name, conditions, cache, profile, data_topic_root, request_topic_root, max_age)
        self.presence_topic_root = presence_topic_root
        self.cache.add_subscription(presence_topic_root + "/+")
        self.cache.add_observer(presence_topic_root + "/+", self )
        self.logger.info("Adding presence topic subscription {0!s}"
                         .format(presence_topic_root))

    def evaluate(self, zone):
        clock = self.get_clock()
        if not zone:
            return False
        data = self.cache.last_for_topic(self.presence_topic_root+"/"+zone)
        if not data:
            return False
        if "timestamp" in data:
            timestamp = float(data["timestamp"])
            if timestamp + self.max_age < clock:
                self.logger.debug("Expired data ignored Now: {1!s} Then: {2!s} = {3!s} - {0!s}".format(data, clock, timestamp, clock-timestamp))
                return False
            else:
                self.logger.debug("Found valid timestamp on presence {0!s}".format(zone))
        else:
            self.logger.warn("WARN: Permitting data without timestamp {0!s}".format(data))
        if "isPresent" in data:
            if data["isPresent"]:
                self.logger.debug("Found valid presence for zone {0!s}".format(zone))
                super().evaluate(zone)
            else:
                self.logger.debug("Found non-present for zone {0!s}".format(zone))
        else:
            self.logger.warn("Invalid presence message, no isPresent")


class SympatheticZoneProfileSchedule(ZoneProfileSchedule):
    def __init__(self, name, conditions, cache, profile,
                 data_topic_root="home/sensors/temperature",
                 request_topic_root="home/heating/requests",
                 max_age=300,
                 demand_topic_root="home/heating/demands"):
        super().__init__(name,conditions,cache,profile,data_topic_root,request_topic_root,max_age)
        # No observers, no triggers on demand.  We aren't a demand processor
        self.demand_topic_root = demand_topic_root
        self.cache.add_subscription(demand_topic_root + "/+")
        self.logger.info("Adding demand topic subscription {0!s}"
                         .format(demand_topic_root))

    def is_valid_demand(self, zone):
        demands:dict = self.cache.last_for_topic_expr(self.demand_topic_root+"/+")
        if demands and len(demands) > 0:
            for key, demand in demands.items():
                demand_zone = str(key.rpartition("/")[2])
                if demand_zone == zone:
                    continue
                demand_obj = Demand(demand, demand_zone)
                if not demand_obj.is_expired(self.get_clock()):
                    if "sympathetic" in demand_obj.data["metadata"]:
                        if demand_obj.data["metadata"]["sympathetic"]:
                            # ignore it
                            continue
                        return True # sympathetic false
                    else:
                        return True # normal demand
                 # expired
            # none left
        # None OR None Left
        return False

    def evaluate_data(self, target, current, zone, clock):
        if not self.is_valid_demand(zone):
            return None
        if "accept_heat" not in target:
            return None
        cache.client.publish_retained("home/heating/active_targets/{0!s}".format(zone), json.dumps({
            "target":target["accept_heat"],
            "timestamp": clock,
            "profile": self.profile or "None",
            "hr_time": time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(clock)),
            "schedule": self.name,
            "sympathetic": True }))

        if current < float(target["accept_heat"]):
            return {
                "timestamp": clock,
                "hr_time": time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(clock)),
                "state": "ON",
                "zone": zone,
                "expiry": 30,
                "metadata": {
                    "schedule": {
                        "name": self.name,
                        "targetProfile": self.profile
                    },
                    "target": float(target["accept_heat"]),
                    "current": current,
                    "sympathetic": True
                }
            }
        return None


class LookAheadZoneTemperatureSchedule(ZoneProfileSchedule):
    def __init__(self, name, conditions, cache, profile, data_topic_root, request_topic_root, look_ahead):
        super().__init__(name, conditions, cache, profile, data_topic_root, request_topic_root)
        self.look_ahead = look_ahead
        self.max_age += look_ahead

    def get_clock(self):
        return super().get_clock() + self.look_ahead


mqtt = MqttClient(client_name="Scheduler")
master_clock = MasterClock(mqtt, "home/clock")
master_clock.subscribe_to_clock()

cache = MqttMultiTopicObservableCache(mqtt, [], None)

all_day_start = datetime.time(hour=6, minute=30, second=0)
all_day_end = datetime.time(hour=23, minute=00, second=0)
all_day = TimeCondition(all_day_start, all_day_end)
all_night = TimeCondition(all_day_end, all_day_start)

weekend = DayOfWeekCondition([6, 7])
weekend_routine_conditions = [("AND", all_day), ("AND", weekend)]

weekday = DayOfWeekCondition([1, 2, 3, 4, 5])

morning_start = datetime.time(hour=7, minute=30, second=0)
morning_end = datetime.time(hour=8, minute=00, second=0)
morning = TimeCondition(morning_start, morning_end)

weekday_morning_routine_conditions = [("AND", weekday), ("AND", morning)]

evening_start = datetime.time(hour=16, minute=0, second=0)
evening_end = datetime.time(hour=23, minute=0, second=0)
evening = TimeCondition(evening_start, evening_end)
weekday_evening_routine_conditions = [("AND", weekday), ("AND", evening)]

weekend_morning_routine_conditions = [("AND", weekend), ("AND", morning)]

maintenance_schedule = ZoneProfileSchedule("Maintenance Schedule", [], cache, "home/heating/target_temps/eco" )

weekend_routine_schedule = ZoneProfileSchedule("Weekend Routine", weekend_routine_conditions, cache, "home/heating/target_temps/eco")
weekend_morning_routine_schedule = ZoneProfileSchedule("Weekend Morning Boost", weekend_morning_routine_conditions, cache, "home/heating/target_temps/morning_boost")

weekday_morning_routine_schedule = ZoneProfileSchedule("Weekday Morning Boost", weekday_morning_routine_conditions, cache, "home/heating/target_temps/morning_boost")

weekday_evening_routine_schedule = ZoneProfileSchedule("Weekday Evening Routine", weekday_evening_routine_conditions, cache, "home/heating/target_temps/eco")


#zts = LookAheadZoneTemperatureSchedule("ZoneTemperatureSchedule", weekday_evening_routine_conditions, cache, "home/heating/target_temps/comfort",
#                                       "home/sensors/temperature", "home/heating/requests/future", look_ahead=3600)

#frp = FutureRequestProcessor(zts.queue)
#frp.handlers.append(RampHandler("office", 2))

sympathetic = SympatheticZoneProfileSchedule("Sympathetic Heating", [], cache, "home/heating/profiles/sympathetic/eco")
presence = PresenceZoneProfileSchedule("Zone Presence", [], cache, "home/heating/target_temps/comfort")
time.sleep(2)
mqtt.blocking_run()
