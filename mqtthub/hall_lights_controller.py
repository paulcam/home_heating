from libs.mqtt_lib import MqttClient, MqttMultiTopicObservableCache
import time
import datetime
import json
import logging

from libs.logging_setup import setup_logging
setup_logging()

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)


solar_panel_power_topic = "solar/panel"
alt_solar_panel_topic = "venus-home/N/b827eb3e8f99/solarcharger/289/Yield/Power"

class HallLightController:
    def __init__(self, client, cache, solar_power_topic):
        global alt_solar_panel_topic
        self.solar_power_topic = solar_power_topic
        self.client = client
        self.cache = cache
        self.upstairs_motion_topic = "zigbee2mqtt/hall_us_motion"
        self.downstairs_motion_topic = "zigbee2mqtt/hall_ds_motion"
        self.bathroom_motion_topic = "zigbee2mqtt/bathroom_motion"
        self.cache.add_subscription( self.upstairs_motion_topic )
        self.cache.add_observer(self.upstairs_motion_topic, self)
        self.cache.add_subscription(self.downstairs_motion_topic)
        self.cache.add_observer(self.downstairs_motion_topic, self)
        self.cache.add_subscription(self.solar_power_topic)
        self.cache.add_subscription(self.bathroom_motion_topic)
        self.cache.add_observer(self.bathroom_motion_topic, self)
        self.cache.add_subscription( alt_solar_panel_topic )

        # No observer for panel voltage.  Does not trigger events.
    def get_panel_power(self):
        global alt_solar_panel_topic
        panel_1_power = self.cache.last_for_topic(self.solar_power_topic)
        panel_2_power = self.cache.last_for_topic(alt_solar_panel_topic)
        print(panel_2_power)
        panel1Value = 0
        panel2Value = 0
        if panel_1_power:
            if "P" in panel_1_power:
                panel1value = float(panel_1_power["P"])
        if panel_2_power:
#            panel_2_power = json.loads(str(panel_2_power))
            panel2Value = float(panel_2_power["value"])
        module_logger.debug("Panel1Value: "+str(panel1Value)+", Panel2Value: "+str(panel2Value))
        return max( panel1Value, panel2Value )


    def notify(self, topic):
        payload = self.cache.last_for_topic(topic)

        if topic == self.upstairs_motion_topic:
            print("Upstairs motion message, payload: {0!s}".format(payload))
            bulbs = ["upstairs_hall_bulb"]

        elif topic == self.downstairs_motion_topic:
            print("Down stairs motion message, payload: {0!s}".format(payload))
            bulbs = ["downstairs_hall_bulb"]
        elif topic == self.bathroom_motion_topic:
            print("Bathroom motion message, payload: {0!s}".format(payload))
            bulbs = [ "bathroom_bulb1", "bathroom_bulb2" ]
        else:
            return

        if "occupancy" in payload:
            occupancy = payload["occupancy"]
            if occupancy:
                panel_power = self.get_panel_power()
                if panel_power > 0.1:
                    print("Still Daylight.")
                    return

                self.turnon(bulbs)
            else:
                self.turnoff(bulbs)

    def turnon(self, bulbs):
        for bulb in bulbs:
            topic = "zigbee2mqtt/{0!s}/set".format(bulb)
            brightness = 127
            transition = 0.5
            now = datetime.datetime.now()
            hour = now.hour
            if hour < 7:
                brightness = 64
                transition = 5

            payload = json.dumps({
                "state": "ON",
                "brightness": brightness,
                "transition": transition
            })
            self.client.publish(topic, payload)

    def turnoff(self, bulbs):
        for bulb in bulbs:
            topic = "zigbee2mqtt/{0!s}/set".format(bulb)
            payload = json.dumps({
                "state": "OFF",
                "transition": 3
            })
            self.client.publish(topic, payload)

def on_clock(client, userdata, message):
    global clock
    clock_obj = json.loads(message.payload.decode("UTF-8"))
    if "utctime" in clock_obj:
        utctime = clock_obj["utctime"]
        clock = float(utctime)

mqtt = MqttClient(broker_address="10.0.0.199", broker_port=1883, client_name="HallLightController")
cache = MqttMultiTopicObservableCache(mqtt, [], "value")
clock = time.time()
mqtt.subscribe("home/clock")
mqtt.message_callback_add("home/clock", on_clock)
time.sleep(5)

hall_cntlr = HallLightController(mqtt, cache, solar_panel_power_topic)

mqtt.blocking_run()
