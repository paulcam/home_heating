import json
import copy
import time

class JSONObject:
    def __init__(self, data, zone):
        self.data = data
        self.zone = zone

    @classmethod
    def from_json(cls, data_str, zone):
        return cls( json.loads(data_str), zone)

    def to_json(self):
        return json.dumps(self.data)

    def __str__(self):
        return self.zone + " " + str(self.data)

    def __repr__(self):
        return "{0!s}: {1!s}".format(type(self).__name__, str(self))

class TimestampedJSONObject(JSONObject):
    def __init__(self, data, zone):
        super().__init__(data, zone)

    def get_timestamp(self):
        if "timestamp" in self.data:
            return self.data["timestamp"]
        return None

    def set_timestamp(self, timestamp):
        self.data["timestamp"] = timestamp
        self.data["hr_time"] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(timestamp))


    def is_stale(self, max_age, clock):
        tstamp = self.get_timestamp()
        if not tstamp:
            return True
        if tstamp + max_age < clock:
            return True
        return False


class ExpirableData(TimestampedJSONObject):
    def __init__(self, data, zone):
        super().__init__(data, zone)

    def when_expires(self, now):
        if not self.data:
            return -1
        if "timestamp" not in self.data:
            return -1
        if "expiry" not in self.data:
            return -1
        now = float(now)
        then = float(self.get_timestamp())
        how_long = float(self.data["expiry"])
        when = then + how_long
        return when

    def is_expired(self, now):
        when = self.when_expires(now)
        if when == -1:
            return True
        if when >= now:
            return False
        else:
            return True

    def add_metadata(self, key, object):
        if not "metadata" in self.data:
            self.data["metadata"] = dict()
        self.data["metadata"][key]=object

class Demand(ExpirableData):
    def __init__(self, data, zone):
        super().__init__(data, zone)

    def target(self):
        if "metadata" in self.data:
            data = self.data["metadata"]
            if "target" in data:
                target = data["target"]
                return target
        return None

    @classmethod
    def from_request(cls, request):
        demand_data = copy.deepcopy(request.data)
        if "metadata" in demand_data:
            metadata = demand_data["metadata"]
            metadata.pop("schedule")
            metadata["request"] = request.data
        else:
            metadata = { "request": request.data}
            demand_data["metadata"] = metadata

        return cls(demand_data, request.zone)

class Request(Demand):
    def __init__(self, data, zone):
        super().__init__(data, zone)


class Control(ExpirableData):
    def __init__(self, data, zone):
        super().__init__(data, zone)


class Presence(ExpirableData):
    def __init__(self, data, zone):
        super().__init__(data, zone)

    def is_present(self):
        if "isPresent" in self.data:
            return bool(self.data["isPresent"])
        else:
            return False
