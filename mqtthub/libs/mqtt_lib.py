import paho.mqtt.client as mqtt
import re
import json
from queue import Queue
import logging
module_logger = logging.getLogger(__name__)

class MqttClient:
    def __init__(self, broker_address="mqtt", broker_port=1883, client_name="Hub" ):
        self.logger = module_logger.getChild(self.__class__.__name__).getChild(client_name)
        self.logger.addHandler(logging.NullHandler())

        self.is_connected = False
        self.broker_address=broker_address
        self.broker_port= broker_port
        self.client_name = client_name
        self.client = mqtt.Client(client_name) #create new instance
        self.client.on_message=self.on_message        #attach function to callback
        self.client.on_disconnect=self.on_disconnect
        self.client.on_connect=self.on_connect
        self.client.connect(broker_address, broker_port) #connect to broker
        self.subscriptions = set()

    def subscribe(self, topic):
        if topic not in self.subscriptions:
            self.logger.info("New Subscription to {0!s}".format(topic))
            self.client.subscribe(topic)
            self.subscriptions.add(topic)
        else:
            self.logger.debug("Existing Subscription to {0!s}".format(topic))

    def message_callback_add(self, topic, callback):
        self.client.message_callback_add(topic, callback)

    def blocking_run(self):
        self.client.loop_forever()

    def threaded_run(self):
        self.client.loop_start()

    def on_message(self, client, userdata, message):
        pass

    def on_disconnect(self, client, userdata, rc):
        self.is_connected = False
        if rc != 0:
            self.logger.error("Unexpected MQTT disconnection. Will auto-reconnect")

    def on_connect(self, client, userdata, flags, rc):
        self.is_connected = True
        self.logger.info("Got connected to {0!s} on port {1!s}".format(self.broker_address, self.broker_port))
        for sub in self.subscriptions:
            client.subscribe(sub)

    def publish(self, topic, payload, retain=False):
        self.logger.debug("MQTT Attempting to publish {0!s} TOPIC: {1!s}".format(payload, topic))
        self.client.publish(topic,payload, retain=retain)

    def publish_retained(self, topic, payload):
        self.publish(topic, payload, retain=True)




class MqttTopicCache:
    def __init__(self, client, topic):
        self.logger = module_logger.getChild(self.__class__.__name__).getChild(topic)
        self.logger.addHandler(logging.NullHandler())
        self.last_messages = dict()
        client.subscribe(topic)
        client.message_callback_add(topic, self.on_topic)

    def on_topic(self, client, userdata, message):
        topic = message.topic
        payload = message.payload.decode("UTF-8")
        self.logger.debug("Cache received on {0!s} {1!s}".format(topic, payload))
        self.last_messages[topic] = message.payload.decode("UTF-8")

    def get_last_for_topic(self, topic):
        if topic in self.last_messages:
            return self.last_messages[topic]
        else:
            return None


class MqttMultiTopicObservableCache:

    def __init__(self, client, topic_list, on_change_field):
        self.logger = module_logger.getChild(self.__class__.__name__)
        self.logger.addHandler(logging.NullHandler())
        self.client = client
        self.topic_list = topic_list
        self.cache = dict()
        self.observers = []
        self.subscribe_all()
        self.on_change_field = on_change_field

    def subscribe_all(self):
        for topic in self.topic_list:
            self.logger.info("Subscribing to {0!s}".format(topic))
            self.client.subscribe(topic)
            self.logger.info("Adding message callback for {0!s}".format(topic))
            self.client.message_callback_add(topic, self.on_message)

    def add_observer(self, topic_expression, observer):
        if not (topic_expression, observer) in self.observers:
            self.logger.info("Adding new observer pair {0!s}".format((topic_expression, observer)))
            self.observers.append((topic_expression, observer))
        else:
            self.logger.debug("Ignoring duplicate observer pair {0!s}".format((topic_expression, observer)))

    def on_message(self, client, userdata, message):
        current = message.payload.decode("UTF-8")
        if len(current) < 1:
            return
        try:
            current = json.loads(current)
            if isinstance(current, int) or isinstance(current,float):
                current_value = current
            elif self.on_change_field in current:
                current_value = current[self.on_change_field]
            else:
                current_value = None
        except:
            current_value = None

        # Changed?
        notify = False
        if message.topic in self.cache:
            previous = self.cache[message.topic]
            if isinstance(previous, int) or isinstance(previous,float):
                previous_value = previous
            elif self.on_change_field in previous:
                previous_value = previous[self.on_change_field]
            else:
                previous_value = None
            if previous_value != current_value or current_value is None or previous_value is None:
                notify = True
        else:
            notify = True

        self.cache[message.topic] = current
        if notify:
            self.notify_observers(message.topic)

    def notify_observers(self, topic):
        for topic_expression, observer in self.observers:
            # https://codegolf.stackexchange.com/questions/188397/mqtt-subscription-topic-match/188417
            topic_reg = topic_expression.translate({43:"[^/]+",35:".+"})+"$"
            if re.match(topic_reg, topic):
                observer.notify(topic)

    def add_subscription(self, topic):
        if topic not in self.topic_list:
            self.logger.info("Adding subscription to topic: {0!s}".format(topic))
            self.client.subscribe(topic)
            self.topic_list.append(topic)
            self.client.message_callback_add(topic, self.on_message)

    def last_for_topic(self, topic):
        if topic in self.cache:
            return self.cache[topic]
        return None

    def last_for_topic_expr(self, topic_expr):
        data = dict()
        for key in self.cache.keys():
            if MqttTopicMatcher.matches(key, topic_expr):
                data[key] = self.cache[key]
        return data

class MqttTopicQueue:
    def __init__(self, client, topic):
        self.logger = module_logger.getChild(self.__class__.__name__)
        self.logger.addHandler(logging.NullHandler())
        self.messages=[]
        self.queue = Queue(100)
        client.subscribe(topic)
        client.message_callback_add(topic, self.on_topic)

    def on_topic(self, client, userdata, message):
        topic = message.topic
        payload = message.payload.decode("UTF-8")
        self.logger.debug("Queue received on {0!s} {1!s}".format(topic, payload))
        self.queue.put(message)

    def pop(self):
        self.logger.debug("Waiting on queued item...")
        message = self.queue.get()
        self.logger.debug("Got queued item... {0!s}".format(message))
        return message.topic, message.payload.decode("UTF-8")


class MqttTopicMatcher:
    @classmethod
    def matches(cls, topic, topic_expression):
        topic_reg = topic_expression.translate({43: "[^/]+", 35: ".+"})
        return re.match(topic_reg, topic)
