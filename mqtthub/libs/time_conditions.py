from abc import ABC, abstractmethod
import datetime
import time

class Condition:
    @abstractmethod
    def evaluate(self, data):
        pass

    @classmethod
    def _or(cls, state, condition, data):
        return state or condition.evaluate(data)

    @classmethod
    def _and(cls, state, condition, data):
        return state and condition.evaluate(data)


class TimeCondition(Condition):
    def __init__(self, startTime, stopTime ):
        self.startTime = startTime
        self.stopTime = stopTime

    def __str__(self):
        return "TimeCondition"

    def evaluate(self, data):
        if "currentTime" in data:
            current_time = datetime.datetime.fromtimestamp(data['currentTime'])
            #print("Found current time: {0!s}".format(str(data['currentTime'])))
            #print("Start time: {0!s} Stop time: {1!s}".format(str(self.startTime), str(self.stopTime) ) )
            if self.in_between(current_time.time(), self.startTime, self.stopTime ):
                #print("We are between those times")
                return True
            else:
                return False
        else:
            print("Current time not found in data")
            return False

    @staticmethod
    def in_between(now, start, end):
        if start <= end:
            return start <= now < end
        else: # over midnight e.g., 23:30-04:15
            return start <= now or now < end


class DayOfWeekCondition(Condition):
    # Monday is 1, Sunday is 7
    def __init__(self, active_days):
        self.active_days = active_days

    def __str__(self):
        return "DayOfWeekCondition Active Days: "+str(self.active_days)

    def evaluate(self, data):
        if "currentTime" in data:
            current_time = datetime.datetime.fromtimestamp(data['currentTime'])
            #print("Found current time: {0!s}".format(str(current_time)))
            #print("Active Days: {0!s}".format(str(self.active_days)))
            if self.is_active_day(current_time.date()):
                #print("currentTime is an active day")
                return True
            else:
                return False
        else:
            print("Current time not found in data")
            return False

    def is_active_day(self, current_date):
        day_of_week = current_date.weekday()+1 # Python days are 0-6
        if day_of_week in self.active_days:
            return True
        return False


class BasicTemperatureCondition(Condition):
    def __init__(self, key, minimum):
        self.minimum = minimum
        self.key = key

    def evaluate(self, data):
        if self.key not in data:
            return False
        temperature = data[self.key]

        if float(temperature) < -50 or float(temperature) > 100:
            print("Invalid temp reading for {} of {}".format(self.key, str(temperature)))
            return False

        print("{0!s}: {1!s} < {2!s}".format(self.key, temperature, self.minimum))
        return float(temperature) < self.minimum
    def __repr__(self):
        return "BasicTemperatureCondition( {0!s}, NIN:{1!s}".format(self.key, self.minimum)


class AnyOfCondition(Condition):
    def __init__(self, conditions):
        self.conditions = conditions

    def evaluate(self, data):
        state = False
        for condition_tuple in self.conditions:
            modifier = condition_tuple[0]
            condition = condition_tuple[1]

            #print("Modifier: {0!s} Condition: {1!s}".format(modifier, condition))
            if modifier == "OR":
                state = Condition._or( state, condition, data )
            elif modifier == "AND":
                state = Condition._and( state, condition, data )
            else:
                print("Unknown modifier: {0!s}".format(modifier))
        return state

    def __str__(self):
        message = "AnyOf "
        sep = ""
        for condition in self.conditions:
            message += "{0!s}{1!s}".format(sep,str(condition))
            sep = ","
        return message