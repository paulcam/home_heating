import json
import logging
import time
from threading import Timer
from libs.logging_setup import setup_logging
from libs.mqtt_lib import MqttClient, MqttMultiTopicObservableCache

soil_t = 20
air_t= 17
hist=2
ext_t=18
max_humidity=70

setup_logging()

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.INFO)


main_temperature_topic = "/sensors/PropagatorTemp1"

soil_temperature_topic = "/sensors/PropagatorTemp2"

aux_temperature_topic = "/sensors/Propagator"

humidity_topic = "/sensors/PropagatorHumidity"

extractor_topic = "cmnd/tasmota_162AD6/POWER1"
extractor_state_topic = "stat/tasmota_162AD6/POWER1"

heater_topic = "cmnd/tasmota_162AD6/POWER3"
heater_state_topic = "stat/tasmota_162AD6/POWER3"

heat_pad_topic =  "cmnd/tasmota_162AD6/POWER2"
heat_pad_state_topic = "stat/tasmota_162AD6/POWER2"

class EnvironmentController:
    def __init__(self, client:MqttClient, cache, main_temperature_topic):
        self.main_temperature_topic = main_temperature_topic
        self.client = client
        self.cache = cache

        self.cache.add_subscription(self.main_temperature_topic)
        self.cache.add_observer(self.main_temperature_topic, self)

        self.cache.add_subscription(soil_temperature_topic)
        self.cache.add_observer(soil_temperature_topic, self)

        self.cache.add_subscription(aux_temperature_topic)
        self.cache.add_observer(aux_temperature_topic, self)

        self.cache.add_subscription(humidity_topic)
        self.cache.add_observer(humidity_topic, self)

        self.cache.add_subscription(extractor_state_topic)
        self.cache.add_observer(extractor_state_topic, self)
        self.cache.add_subscription(heater_state_topic)
        self.cache.add_observer(heater_state_topic, self)
        self.cache.add_subscription(heat_pad_state_topic)
        self.cache.add_observer(heat_pad_state_topic, self)
        self.request_state_forever()

    def request_state_forever(self):
        module_logger.info("Requesting state refresh from power strip")
        # Forces a status update on the stat topics.
        self.client.publish(extractor_topic, "")
        self.client.publish(heater_topic, "")
        self.client.publish(heat_pad_topic, "")
        Timer(60, self.request_state_forever).start()

    def notify(self, topic):
        payload = self.cache.last_for_topic(topic)

        if topic == aux_temperature_topic:
            module_logger.debug("Temperature {1!s} message, payload: {0!s}".format(payload, topic))
            temp = float(payload["value"])
            module_logger.info("Ambient Temp: {0!s}".format(temp))
            if temp < ext_t:
                self.extractor("OFF") # if it gets really cold and heater can't keep up
            if temp > ext_t+hist:
#                if datetime.time(11,00) <= datetime.datetime.now().time() <= datetime.time(23,00):
#                  self.extractor("OFF")
#                else:
                self.extractor("ON")
            if temp < air_t:
                self.heater("ON")
            if temp > air_t+hist:
                self.heater("OFF")

        elif topic == soil_temperature_topic:
            module_logger.debug("Soil Temperature {1!s} message, payload: {0!s}".format(payload, topic))

            temp = float(payload["value"])
            module_logger.info("Soil Temp:\t\t{0!s}".format(temp))
            if temp < soil_t:
                self.heat_pad("ON")
            if temp > soil_t+1:
                self.heat_pad("OFF")

        elif topic == humidity_topic:
            module_logger.debug("Humidity {1!s} message, payload: {0!s}".format(payload, topic))

            humidity = float(payload["value"])
            module_logger.info("Humidity:\t\t{0!s}".format(humidity))
            if humidity > max_humidity:
                self.extractor("ON")
        else:
            module_logger.debug("Message on {0!s} ignored: {1!s}".format(topic, payload))

    def heater(self, on):
        heater_state = self.cache.last_for_topic(heater_state_topic)  or "OFF"
        module_logger.debug("Current heater State:"+heater_state)
        if heater_state == on == "ON":
            return
        module_logger.info("Switching heater {0!s}".format(on))

        if on == "ON":
            self.client.publish(heater_topic, "ON")
        else:
            self.client.publish(heater_topic, "OFF")

    def heat_pad(self, on):
        heat_pad_state = self.cache.last_for_topic(heat_pad_state_topic)  or "OFF"
        module_logger.debug("Current Heat Pad State:"+heat_pad_state)
        if heat_pad_state == on == "ON":
            return
        module_logger.info("Switching heatpad {0!s}".format(on))
        if on == "ON":
            self.client.publish(heat_pad_topic, "ON")
        else:
            self.client.publish(heat_pad_topic, "OFF")

    def extractor(self, on):
        extractor_state = self.cache.last_for_topic(extractor_state_topic) or "OFF"
        module_logger.debug("Current Extractor State:"+extractor_state)
        if extractor_state == on == "ON":
            return
        module_logger.info("Switching extracter {0!s}".format(on))
        if on == "ON":
            self.client.publish(extractor_topic, "ON")
        else:
            self.client.publish(extractor_topic, "OFF")

def on_clock(client, userdata, message):
    global clock
    clock_obj = json.loads(message.payload.decode("UTF-8"))
    if "utctime" in clock_obj:
        utctime = clock_obj["utctime"]
        clock = float(utctime)

mqtt = MqttClient(broker_port=1883, client_name="EnvironmentControllerTest")
cache = MqttMultiTopicObservableCache(mqtt, [], "value")
clock = time.time()
mqtt.subscribe("home/clock")
mqtt.message_callback_add("home/clock", on_clock)
time.sleep(5)

hall_cntlr = EnvironmentController(mqtt, cache, main_temperature_topic)

mqtt.blocking_run()
