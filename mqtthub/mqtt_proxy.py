from libs.mqtt_lib import MqttClient
from libs.value_objects import TimestampedJSONObject
import time
import json



def on_clock(client, userdata, message):
    global clock
    clock_obj = json.loads(message.payload.decode("UTF-8"))
    if "utctime" in clock_obj:
        utctime = clock_obj["utctime"]
        clock = float(utctime)
        if clock % 5 == 0:
            publish_client: MqttClient = userdata["prod"]
            publish_client.publish("venus-home/R/b827eb3e8f99/keepalive", "")


def republish_sonoff_temp_humidity(client, userdata, message, topic_base):
    global clock
    if len(message.payload) < 1:
        return
    incoming_topic = message.topic
    zone = message.topic


def republish_client(client, userdata, message):
    pass
    client.publish(message.topic, message.payload)


def sonoff_sensor_publish(client, userdata, message, zone, name):
    global clock
    publish_client: MqttClient = userdata["internal"]
    try:
        json_obj = TimestampedJSONObject(json.loads(message.payload.decode("UTF-8")), zone)
    except:
        pass
    json_obj.data["key"] = zone
    json_obj.data["name"] = name

    json_obj.set_timestamp(clock)
    json_obj.data["value"] = json_obj.data["temperature"]

    payload = json.dumps(json_obj.data)
    #print(payload)
    publish_client.publish_retained("home/sensors/temperature/"+zone, payload)

    json_obj.data["value"] = json_obj.data["humidity"]

    payload = json.dumps(json_obj.data)
   # print(payload)
    publish_client.publish_retained("home/sensors/humidity/"+zone, payload)


def sonoff_sensor_detect(client, userdata, message):
    topic = str(message.topic).rpartition("/")[2]
    if str(topic).endswith("_th"):
        zone = str(topic).rpartition("_")[0]
        sonoff_sensor_publish(client, userdata, message, zone, zone.capitalize())

def tasmota_power_monitors(client,userdata,message):
    global clock
    publish_client: MqttClient = userdata["internal"]
    monitor_name = str(message.topic).split("/")[-2]
    try:
        json_obj = TimestampedJSONObject(json.loads(message.payload.decode("UTF-8")), monitor_name)
        json_obj.set_timestamp(clock)

        # Energy?
        if "ENERGY" in json_obj.data:

            energy = json_obj.data["ENERGY"]
            # Power
            if "Power" in energy:
                key = monitor_name+"_power"
                json_obj.data["key"] = key
                json_obj.data["name"] = key
                json_obj.data["units"] = "W"
                json_obj.data["value"] = energy["Power"]
                payload = json.dumps(json_obj.data)
                #print(payload)
                publish_client.publish_retained("home/sensors/power/" + key, payload)
            if "Voltage" in energy:
                key = monitor_name+"_voltage"
                json_obj.data["key"] = key
                json_obj.data["name"] = key
                json_obj.data["units"] = "V"
                json_obj.data["value"] = energy["Voltage"]
                payload = json.dumps(json_obj.data)
                #print(payload)
                publish_client.publish_retained("home/sensors/power/" + key, payload)
            if "Factor" in energy:
                key = monitor_name+"_factor"
                json_obj.data["key"] = key
                json_obj.data["name"] = key
                json_obj.data["units"] = "%"
                json_obj.data["value"] = energy["Factor"]
                payload = json.dumps(json_obj.data)
                #print(payload)
                publish_client.publish_retained("home/sensors/power/" + key, payload)
            if "Total" in energy:
                key = monitor_name+"_total"
                json_obj.data["key"] = key
                json_obj.data["name"] = key
                json_obj.data["units"] = "kWh"
                json_obj.data["value"] = energy["Total"]
                payload = json.dumps(json_obj.data)
                #print(payload)
                publish_client.publish_retained("home/sensors/power/" + key, payload)
    except Exception as e:
        print( "Exception in tasmota mqtt message: {0!s} --- {1!s}", payload, e)

def solar_publish(client, userdata, message):
    global clock
    if len(message.payload) < 1 or message.topic.endswith("LWT"):
        return
    if not (message.topic.endswith("panel") or message.topic.endswith("load") or message.topic.endswith("battery") or message.topic.endswith("energy")):
        return
    zone="solar"
    topic = "home/sensors/solar/"
    publish_client: MqttClient = userdata["internal"]
    try:        
        json_msg = json.loads(message.payload.decode("UTF-8"))
        json_out = TimestampedJSONObject(dict(), zone)
        json_out.set_timestamp(clock)
    except Exception as e:
        print("Exception in solar mqtt message: {0!s}", e)
        return



    if message.topic.endswith("/panel"):
        topic += "panel"
        json_out.data["value"] = json_msg["V"]
        json_out.data["units"] = "V"
        json_out.data["key"] = "panelVolts"
        json_out.data["name"] = "Panel Volts"
        publish_client.publish_retained(topic+"Volts", json_out.to_json())

        load_amps = json_msg["I"]
        json_out.data["value"] = json_msg["I"]
        json_out.data["units"] = "A"
        json_out.data["key"] = "panelCurrent"
        json_out.data["name"] = "Panel Current"
        publish_client.publish_retained(topic + "Current", json_out.to_json())

        load_power = json_msg["P"]
        json_out.data["value"] = json_msg["P"]
        json_out.data["units"] = "W"
        json_out.data["key"] = "panelPower"
        json_out.data["name"] = "Panel Power"
        publish_client.publish_retained(topic + "Power", json_out.to_json())

    if message.topic.endswith("/load"):
        topic += "load"
        json_out.data["value"] = json_msg["V"]
        json_out.data["units"] = "V"
        json_out.data["key"] = "loadVolts"
        json_out.data["name"] = "Load Volts"
        publish_client.publish_retained(topic+"Volts", json_out.to_json())

        load_amps = json_msg["I"]
        json_out.data["value"] = json_msg["I"]
        json_out.data["units"] = "A"
        json_out.data["key"] = "loadCurrent"
        json_out.data["name"] = "Load Current"
        publish_client.publish_retained(topic + "Current", json_out.to_json())

        load_power = json_msg["P"]
        json_out.data["value"] = json_msg["P"]
        json_out.data["units"] = "W"
        json_out.data["key"] = "loadPower"
        json_out.data["name"] = "Load Power"
        publish_client.publish_retained(topic + "Power", json_out.to_json())

    if message.topic.endswith("/battery"):
        topic += "battery"
        json_out.data["value"] = json_msg["V"]
        json_out.data["units"] = "V"
        json_out.data["key"] = "batteryVolts"
        json_out.data["name"] = "Battery Volts"
        publish_client.publish_retained(topic+"Volts", json_out.to_json())

        load_amps = json_msg["I"]
        json_out.data["value"] = json_msg["I"]
        json_out.data["units"] = "A"
        json_out.data["key"] = "batteryCurrent"
        json_out.data["name"] = "Battery Current"
        publish_client.publish_retained(topic + "Current", json_out.to_json())

        load_power = json_msg["P"]
        json_out.data["value"] = json_msg["P"]
        json_out.data["units"] = "W"
        json_out.data["key"] = "batteryPower"
        json_out.data["name"] = "Battery Power"
        publish_client.publish_retained(topic + "Power", json_out.to_json())

    if message.topic.endswith("/energy") or message.topic.endswith("/energy2"):
        topic += "energy/"
        json_out.data["value"] = json_msg["consumed_day"]
        json_out.data["units"] = "kWh"
        json_out.data["key"] = "energyConsumedDay"
        json_out.data["name"] = "Energy Consumed Day"
        publish_client.publish_retained(topic+"consumed_day", json_out.to_json())

        json_out.data["value"] = json_msg["consumed_month"]
        json_out.data["units"] = "kWh"
        json_out.data["key"] = "energyConsumedMonth"
        json_out.data["name"] = "Energy Consumed Month"
        publish_client.publish_retained(topic+"consumed_month", json_out.to_json())

        json_out.data["value"] = json_msg["consumed_year"]
        json_out.data["units"] = "kWh"
        json_out.data["key"] = "energyConsumedYear"
        json_out.data["name"] = "Energy Consumed Year"
        publish_client.publish_retained(topic+"consumed_year", json_out.to_json())

        json_out.data["value"] = json_msg["consumed_all"]
        json_out.data["units"] = "kWh"
        json_out.data["key"] = "energyConsumedAll"
        json_out.data["name"] = "Energy Consumed All"
        publish_client.publish_retained(topic+"consumed_all", json_out.to_json())

        json_out.data["value"] = json_msg["generated_day"]
        json_out.data["units"] = "kWh"
        json_out.data["key"] = "energyGeneratedDay"
        json_out.data["name"] = "Energy Generated Day"
        publish_client.publish_retained(topic+"generated_day", json_out.to_json())

        json_out.data["value"] = json_msg["generated_month"]
        json_out.data["units"] = "kWh"
        json_out.data["key"] = "energyGeneratedMonth"
        json_out.data["name"] = "Energy Generated Month"
        publish_client.publish_retained(topic+"generated_month", json_out.to_json())

        json_out.data["value"] = json_msg["generated_year"]
        json_out.data["units"] = "kWh"
        json_out.data["key"] = "energyGeneratedYear"
        json_out.data["name"] = "Energy Generated Year"
        publish_client.publish_retained(topic+"generated_year", json_out.to_json())

        json_out.data["value"] = json_msg["generated_all"]
        json_out.data["units"] = "kWh"
        json_out.data["key"] = "energyGeneratedAll"
        json_out.data["name"] = "Energy Generated All"
        publish_client.publish_retained(topic+"generated_all", json_out.to_json())

        json_out.data["value"] = float(json_msg["generated_day"]) - float(json_msg["consumed_day"])
        json_out.data["units"] = "kWh"
        json_out.data["key"] = "energyNetDay"
        json_out.data["name"] = "Energy Net Day"
        publish_client.publish_retained(topic+"net_day", json_out.to_json())


def jk_bms(client, userdata, message):
    global clock
    if len(message.payload) < 1 or message.topic.endswith("LWT"):
        return

    zone = "main-battery"
    topic = "home/sensors/main-battery/"
    publish_client: MqttClient = userdata["internal"]

    #json_msg = json.loads(message.payload.decode("UTF-8"))

    json_out = TimestampedJSONObject(dict(), zone)
    json_out.set_timestamp(clock)
    sensor_topic = str(message.topic).split("/")[-2]
    sensor_topic = sensor_topic.replace("main-battery_", "")
    topic += sensor_topic

    if sensor_topic.startswith("cell_"):
        cell_number = sensor_topic.split("_")[-1]
        if int(cell_number) > 8:
            return

    if "voltage" in sensor_topic:
        json_out.data["units"] = "V"
        value = float(message.payload)
    elif "current" in sensor_topic:
        json_out.data["units"] = "A"
        value = float(message.payload)
    elif "power" in sensor_topic:
        json_out.data["units"] = "W"
        value = float(message.payload)
    elif "temperature" in sensor_topic:
        json_out.data["units"] = "C"
        value = float(message.payload)
    elif "state_of_charge" in sensor_topic:
        json_out.data["units"] = "%"
        value = float(message.payload)
    elif "capacity" in sensor_topic:
        json_out.data["units"] = "Ah"
        value = float(message.payload)
    elif "total_runtime" in sensor_topic and "formatted" not in sensor_topic:
        json_out.data["units"] = "S"
        value = float(message.payload)
    elif "resistance" in sensor_topic:
        json_out.data["units"] = "mR"
        value = float(message.payload)
    elif "errors_bitmask" in sensor_topic:
        json_out.data["units"] = "flags"
        value = float(message.payload)       
    else:
        json_out.data["units"] = "none"
        value = message.payload.decode("UTF-8")

    json_out.data["value"] = value
    json_out.data["key"] = sensor_topic
    json_out.data["name"] = sensor_topic
    #print(json_out.to_json())
    publish_client.publish_retained(topic, json_out.to_json())

def on_solar_charger(client, userdata, message):
    global clock
    if len(message.payload) < 1 or message.topic.endswith("LWT"):
        return

    if "solarcharger/289/" in message.topic:
        zone = "mppt2"
    elif "solarcharger/290/" in message.topic:
        zone = "mppt3"
    else:
        zone = "unknown"

    base_topic = "home/sensors/solar/"+zone+"/"
    publish_client: MqttClient = userdata["internal"]

    json_msg = json.loads(message.payload.decode("UTF-8"))

    json_out = TimestampedJSONObject(json_msg, zone)
    json_out.set_timestamp(clock)

    if "/Pv/" in message.topic:
        if message.topic.endswith("/V"):
            topic = base_topic + "panelVolts"
            json_out.data["key"] = "panelVolts"
            json_out.data["name"] = "Voltage"
            json_out.data["units"] = "Volts"
        else:
            return
    elif "/Dc/0/" in message.topic:
        if message.topic.endswith("/Voltage"):
            topic = base_topic + "batteryVolts"
            json_out.data["key"] = "batteryVolts"
            json_out.data["name"] = "Voltage"
            json_out.data["units"] = "Volts"
        elif message.topic.endswith("/Current"):
            topic = base_topic + "batteryCurrent"
            json_out.data["key"] = "batteryCurrent"
            json_out.data["name"] = "Current"
            json_out.data["units"] = "Amps"
        else:
            return
    elif "/Yield/" in message.topic:
        if message.topic.endswith("/Power"):
            topic = base_topic + "batteryPower"
            json_out.data["key"] = "batteryPower"
            json_out.data["name"] = "Power"
            json_out.data["units"] = "Watts"
        else:
            return
    elif "/History/Daily/0/" in message.topic:
        if message.topic.endswith("/Yield"):
            topic = base_topic + "energy/generated_day"
            json_out.data["key"] = "generated_day"
            json_out.data["name"] = "Generated Day"
            json_out.data["units"] = "kWh"
        else:
            return

    else:
        return
    publish_client.publish_retained(topic, json_out.to_json())


def on_vebus(client, userdata, message):
    global clock
    if len(message.payload) < 1 or message.topic.endswith("LWT"):
        return

    zone = "multiplus"
    base_topic = "home/sensors/multiplus/"
    publish_client: MqttClient = userdata["internal"]

    json_msg = json.loads(message.payload.decode("UTF-8"))

    json_out = TimestampedJSONObject(json_msg, zone)
    json_out.set_timestamp(clock)

    if "Ac/Out/L1/" in message.topic  or "Ac/ActiveIn/L1/" in message.topic:
        if "Ac/Out/L1/" in message.topic:
            topic = base_topic + "ACOut/"
        if "Ac/ActiveIn/L1/" in message.topic:
            topic = base_topic + "ACActiveIn/"
        if message.topic.endswith("/F"):
            topic = topic + "frequency"
            json_out.data["key"] = "frequency"
            json_out.data["name"] = "Frequency"
            json_out.data["units"] = "Hz"
        elif message.topic.endswith("/I"):
            topic = topic + "current"
            json_out.data["key"] = "current"
            json_out.data["name"] = "Current"
            json_out.data["units"] = "Amps"
        elif message.topic.endswith("/P"):
            topic = topic + "power"
            json_out.data["key"] = "power"
            json_out.data["name"] = "Power"
            json_out.data["units"] = "Watts"
        elif message.topic.endswith("/V"):
            topic = topic + "voltage"
            json_out.data["key"] = "voltage"
            json_out.data["name"] = "Voltage"
            json_out.data["units"] = "Volts"
        elif message.topic.endswith("/S"):
            topic = topic + "rating"
            json_out.data["key"] = "rating"
            json_out.data["name"] = "Rating"
            json_out.data["units"] = "VA"          
        else:
            return
    elif "Dc/0/" in message.topic:
        topic = base_topic + "DC/"
        if message.topic.endswith("/Current"):
            topic = topic + "current"
            json_out.data["key"] = "current"
            json_out.data["name"] = "Current"
            json_out.data["units"] = "Amps"
        elif message.topic.endswith("/Power"):
            topic = topic + "power"
            json_out.data["key"] = "power"
            json_out.data["name"] = "Power"
            json_out.data["units"] = "Watts"
        elif message.topic.endswith("/Voltage"):
            topic = topic + "voltage"
            json_out.data["key"] = "voltage"
            json_out.data["name"] = "Voltage"
            json_out.data["units"] = "Volts"
        else:
            return
    elif "/288/Energy/" in message.topic:
        topic = base_topic + "Energy/"
        theKey = str(message.topic).rpartition("/")[2]
        topic = topic + theKey
        json_out.data["key"] = theKey
        json_out.data["name"] = theKey
        json_out.data["units"] = "kWh"
    else:
        return
    #json_out.data["value"] = js
    publish_client.publish_retained(topic, json_out.to_json())

def on_mppt_total( client, userdata, message ):
    global clock
    publish_client: MqttClient = userdata["internal"]
    json_out = TimestampedJSONObject(dict(), "solar")
    json_out.set_timestamp(clock)
    json_out.data["key"]="mppt_total"
    json_out.data["name"]="MPPT Total"
    json_out.data["units"]="W"
    json_out.data["value"]= float(message.payload)
    publish_client.publish_retained( "home/sensors/solar/mppt_total", json_out.to_json())
    
def on_power_limiter( client, userdata, message ):
    global clock
    publish_client: MqttClient = userdata["internal"]
    json_out = TimestampedJSONObject(dict(), "solar")
    json_out.set_timestamp(clock)
    json_out.data["key"]="battery_power_limit"
    json_out.data["name"]="Battery Power Limit"
    json_out.data["units"]="W"
    json_out.data["value"]= float(message.payload)
    publish_client.publish_retained( "home/sensors/solar/battery_power_limit", json_out.to_json())

# master MQTT
clock = time.time()
mqtt_prod = MqttClient(broker_address="10.0.0.3", broker_port=1883, client_name="MQTTProxyTest")
mqtt_prod.threaded_run()

# client MQTT
mqtt_internal = MqttClient(broker_address="10.0.0.3", broker_port=11883, client_name="MQTTProxyTest")
mqtt_internal.threaded_run()

mqtt_prod.client.user_data_set(dict({"internal": mqtt_internal, "prod": mqtt_prod}))
mqtt_internal.client.user_data_set(dict({"internal": mqtt_internal, "prod": mqtt_prod}))


mqtt_internal.subscribe("home/clock")
mqtt_internal.message_callback_add("home/clock", on_clock)

# Zigbee-SonOff-TH sensors
mqtt_prod.subscribe("zigbee2mqtt/+")
mqtt_prod.message_callback_add("zigbee2mqtt/+", sonoff_sensor_detect)

# Tasmota power monitor sensors
mqtt_prod.subscribe("home/sockets/em/+/SENSOR")
mqtt_prod.message_callback_add("home/sockets/em/+/SENSOR", tasmota_power_monitors)

mqtt_prod.subscribe("solar/+")
mqtt_prod.message_callback_add("solar/+", solar_publish)

mqtt_prod.subscribe("main-battery/sensor/#")
mqtt_prod.message_callback_add("main-battery/sensor/#", jk_bms)

mqtt_prod.subscribe("venus-home/N/b827eb3e8f99/vebus/288/#")
mqtt_prod.message_callback_add("venus-home/N/b827eb3e8f99/vebus/288/#", on_vebus)

mqtt_prod.subscribe("venus-home/N/b827eb3e8f99/solarcharger/#")
mqtt_prod.message_callback_add("venus-home/N/b827eb3e8f99/solarcharger/#", on_solar_charger)

mqtt_prod.subscribe("home/mppt_total")
mqtt_prod.message_callback_add("home/mppt_total", on_mppt_total)

mqtt_prod.subscribe("home/battery_net_charge")
mqtt_prod.message_callback_add("home/battery_net_charge", on_power_limiter)


while True:
    time.sleep(10)
