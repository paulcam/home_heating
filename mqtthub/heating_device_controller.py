from libs.mqtt_lib import MqttClient, MqttMultiTopicObservableCache
from libs.master_clock import MasterClock
from libs.value_objects import Control
import time
import json
import logging
import requests
from threading import Thread

from libs.logging_setup import setup_logging
setup_logging()

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)

class Controller:
    def __init__(self, client, control_topic):
        self.client = client
        self.cache = MqttMultiTopicObservableCache(client, [control_topic], "timestamp")
        self.control_topic = control_topic
        self.cache.add_observer(control_topic, self)
        self.logger = module_logger.getChild(self.__class__.__name__)
        self.logger.addHandler(logging.NullHandler())
        self.current = 0

    def notify(self, topic):
        try:
            clock = self.get_clock()
            zone = str(topic).rpartition( "/")[2]
            current_control = self.cache.last_for_topic(topic)
            if not current_control:
                self.logger.warning("No current control on topic: {0!s}".format(topic))
                self.poweroff()
                return
            else:
                current_control = Control(current_control, zone)

            if current_control.is_expired(clock):
                self.logger.debug("Current control expired. {0!s}".format(current_control))
                self.poweroff()
                return
            else:
                self.logger.info("Valid control {0!s} now {1!s} control ts: {2!s}".format(current_control, self.get_clock(), current_control.get_timestamp()))

            status = self.get_status()
            self.logger.debug("Current status is {0!s}".format(status))
            control_state = str(current_control.data["state"]).lower()

            #if status != control_state:
            #    self.logger.debug("Changing state to {0!s}".format(control_state))

            if self.set_state(control_state):
                self.logger.info("Successfully set state to {0!s}".format(control_state))
            else:
                self.logger.warning("Failed to set state to {0!s}".format(control_state))
        except Exception as e:
            module_logger.error("Exception in notify: {0!s}".format(e.__class__), e)
            return

    def set_state(self, state):
        pass

    def poweroff(self):
        pass

    def get_status(self):
        pass

    # This refreshes the device state, so that expired control messages result in
    # the device being switched off.
    def run(self):

        while True:
            try:
                time.sleep(30)
                if self.get_clock() - self.current > 29:
                    self.notify(self.control_topic)
                    self.current = self.get_clock()

                # if time.time() - self.current > 400: # TODO this has timezone implications!
                #     self.logger.warn("Forcing heating off as real time is well beyond expiry")
                #     self.poweroff()
                #     self.current = self.get_clock()
            except Exception as e:
                self.logger.error("Exception in {0!s} controller run".format(self.control_topic))


    def publish_status(self, control_state):
        pass

    def get_clock(self):
        return MasterClock.instance.get_current_time()


class HttpController(Controller):
    def __init__(self, client, control_topic, ip_address ):
        super().__init__(client, control_topic)
        self.ip_address = ip_address

    def set_state(self, state):
        try:
            state = str(state).lower()
            url = "http://{0!s}/{1!s}".format(self.ip_address, state)
            resp = requests.get(url)
            #return True
            if resp.status_code != 200:
                self.logger.error("Failed to set  state to {0!s} with url {1!s}".format(state, url))
                return False
            else:
                self.logger.info("Successfully set  state to {0!s} with url {1!s}".format(state, url))
                return True
        except Exception as e:
            module_logger.error("Exception setting state req/res: {0!s}".format(e.__class__), e)
            return False

    def poweroff(self):
        self.logger.info("Powering off.")
        if self.get_status().lower() != "off":
            self.set_state("off")

    def get_status(self):
        try:
            url = "http://{0!s}/status".format(self.ip_address)
            self.logger.info("Getting  status on url {0!s}".format(url))
            resp = requests.get(url)
            self.logger.debug("Response code {1!s} on url {0!s}".format(url, resp.status_code))
            status_json = resp.content
            self.logger.debug("Status JSON {0!s}".format(status_json))
            status_obj = json.loads(status_json)
            status = status_obj["status"]
            return status
        except Exception as e:
            module_logger.error("Exception processing status req/res: {0!s}".format(e.__class__), e)
            return "fail"

class RadiatorController(HttpController):
    def __init__(self, client, control_topic, ip_address, nc=True):
        super().__init__(client, control_topic, ip_address)
        self.nc = nc

    def set_state(self, state):
        if state.lower() == "on" or state.lower() == "open":
            if self.nc:
                return super().set_state("on")
            else:
                return super().set_state("off")

        if state.lower() == "off" or state.lower() == "closed":
            if self.nc:
                return super().set_state("off")
            else:
                return super().set_state("on")


mqtt = MqttClient(broker_port=1883, client_name="DeviceController")
cache = MqttMultiTopicObservableCache(mqtt, [], "value")
master_clock = MasterClock(mqtt, "home/clock")
master_clock.subscribe_to_clock()


time.sleep(2)

boiler_controller = HttpController(mqtt,"home/heating/control/boiler/main", "10.0.0.9")
boiler_controller_thread = Thread(target=boiler_controller.run)
boiler_controller_thread.start()

# These should auto discover.
# Do they ALL need to be threads?  Can we not have one worker thread do them all?
#     Yes, but if that one thread blocks on an Http call that times out....
office_rad_controller = RadiatorController(mqtt,"home/heating/control/radiator/office", "10.0.0.23")
office_rad_controller_thread = Thread(target=office_rad_controller.run)
office_rad_controller_thread.start()

bedroom_rad_controller = RadiatorController(mqtt,"home/heating/control/radiator/bedroom", "10.0.0.22")
bedroom_rad_controller_thread = Thread(target=bedroom_rad_controller.run)
bedroom_rad_controller_thread.start()

lroom_rad_controller = RadiatorController(mqtt,"home/heating/control/radiator/livingRoom", "10.0.0.21")
lroom_rad_controller_thread = Thread(target=lroom_rad_controller.run)
lroom_rad_controller_thread.start()

time.sleep(2)
mqtt.blocking_run()

