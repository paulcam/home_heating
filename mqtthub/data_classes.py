from dataclasses import dataclass
import orjson

@dataclass
class HeatingProfileZoneEntry:
    demand_temp: float
    accept_temp: float
    override_temp: float = 0
    override_valid_until: int = 0

    def effective_demand(self, now):
        if self.override_valid_until > now:
            return self.override_temp
        else:
            return self.demand_temp


livingRoom = HeatingProfileZoneEntry(14, 16)
print(orjson.dumps(livingRoom))

livingRoom2 = orjson.loads('{"demand_temp":14,"accept_temp":16}')
print(orjson.dumps(livingRoom2))

livingroom3 = HeatingProfileZoneEntry(**livingRoom2)
print(livingroom3)


