from libs.mqtt_lib import MqttClient
import time
import json
from libs.master_clock import MasterClock
import logging
from libs.logging_setup import setup_logging
setup_logging()

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)



def publish_target_profiles(mqtt):
    with open('static/default_targets.json', 'r') as file:
        json_txt = file.read()
        json_obj = json.loads(json_txt)
        for profile_key in json_obj.keys():
            profile = json_obj[profile_key]
            for zone_key in profile.keys():
                zone = profile[zone_key]
                target= {}
                target["value"] = zone
                target_json = json.dumps(target)
                topic = "home/heating/target_temps/{0!s}/{1!s}".format(profile_key, zone_key)
                print("Publishing {0!s} to topic {1!s}".format(target_json, topic))
                mqtt.publish_retained(topic, target_json)
    with open('static/sympathetic_targets.json', 'r') as file:
        json_txt = file.read()
        json_obj = json.loads(json_txt)
        for profile_key in json_obj.keys():
            profile = json_obj[profile_key]
            for zone_key in profile.keys():
                zone = profile[zone_key]
                target["value"] = zone["demand_heat"]
                target["accept_heat"] = zone["accept_heat"]
                target_json = json.dumps(target)
                topic = "home/heating/profiles/sympathetic/{0!s}/{1!s}".format(profile_key, zone_key)
                print("Publishing {0!s} to topic {1!s}".format(target_json, topic))
                mqtt.publish_retained(topic, target_json)

def publish_static_data(mqtt):
    publish_target_profiles(mqtt)


class ManagerMqttClient(MqttClient):
    def __init__(self, broker_address="mqtt", broker_port=1883, client_name="Hub" ):
        super().__init__(broker_address, broker_port, client_name)
        self.logger = module_logger.getChild(self.__class__.__name__).getChild(client_name)
        self.logger.addHandler(logging.NullHandler())
        self.client.user_data_set(self)


    def on_connect(self, client, userdata, flags, rc):
        super().on_connect(userdata, userdata, flags, rc)
        publish_static_data(self)




mqtt = ManagerMqttClient(client_name="Manager")
mqtt.threaded_run()

while( not mqtt.is_connected ):
    print("Waiting on MQTT connection")
    time.sleep(1)
clock = MasterClock(mqtt, "home/clock")
clock.generate_clock()
clock.subscribe_to_clock()

publish_static_data(mqtt)

while(True):
    time.sleep(1)
    print("Clock: {0!s}".format(MasterClock.instance.get_current_time()))
