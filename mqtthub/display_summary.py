import math
import json
import logging
import time
import re
from datetime import datetime

from libs.logging_setup import setup_logging
from libs.master_clock import MasterClock
from libs.mqtt_lib import MqttClient
from libs.value_objects import ExpirableData

setup_logging()

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)
mqtt = MqttClient(broker_address="10.0.0.199", broker_port=11883, client_name="DashStats")

temps = dict()
rh = dict()
stats = dict()

internal_zones = ["bedroom", "livingRoom", "hall", "kitchen", "office"]
pub_time = 0


def update_stats():
    global stats
    # Max zone
    for zone in sorted(temps, key=temps.get, reverse=False):
        if zone in internal_zones:
            stats["max_temp_zone"] = zone
            stats["max_temp"] = temps[zone]
    # Min zone
    for zone in sorted(temps, key=temps.get, reverse=True):
        if zone in internal_zones:
            stats["min_temp_zone"] = zone
            stats["min_temp"] = temps[zone]

    if "outdoorTemp" in temps:
        stats["outdoor_temp"] = temps["outdoorTemp"]

    if "heatingOutPipe" in temps:
        stats["heating_temp"] = temps["heatingOutPipe"]

    for zone in sorted(rh, key=rh.get, reverse=False):
        if zone in internal_zones:
            stats["indoor_rh_zone"] = zone
            stats["max_rh"] = rh[zone]

    if "humidity" in rh:
        stats["outdoorRH"] = rh["humidity"]

    if time.time() - update_stats.pub_time > 1:
        json_str = json.dumps(stats)
        mqtt.publish("home/stats/summary", json_str)
        update_stats.pub_time = time.time()
        stats = dict()


def debug_stats():
    pass


def update_rh(zone, temp):
    if zone not in rh:
        rh[zone] = float(temp)
        update_stats()
        return
    if rh[zone] == temp:
        return
    rh[zone] = float(temp)
    update_stats()


def update_temps(zone, temp):
    if zone not in temps:
        temps[zone] = float(temp)
        update_stats()
        return
    if temps[zone] == temp:
        return
    temps[zone] = float(temp)
    update_stats()


def update_power(power):
    stats["power"] = power


def on_power_message(client, user_data, message):
    topic = message.topic
    payload = message.payload.decode("UTF-8")
    datum = json.loads(payload)
    if "value" in datum:
        value = datum["value"]
        update_power(value)


def on_temp_message(client, user_data, message):
    topic = message.topic
    payload = message.payload.decode("UTF-8")
    zone = topic.rpartition("/")[-1]
    datum = json.loads(payload)
    if "value" in datum:
        value = datum["value"]
        update_temps(zone, value)

def on_rh_message(client, user_data, message):
    topic = message.topic
    payload = message.payload.decode("UTF-8")
    zone = topic.rpartition("/")[-1]
    datum = json.loads(payload)
    if "value" in datum:
        value = datum["value"]
        update_rh(zone, value)


def on_solar_message(client, user_data, message):
    topic = message.topic
    payload = message.payload.decode("UTF-8")
    zone = topic.rpartition("/")[-1]
    datum = json.loads(payload)
    if zone == 'panelPower':
        if "value" in datum:
            value = datum["value"]
            stats["solar"] = value
    if zone == 'batteryVolts':
        if "value" in datum:
            value = datum["value"]
            stats["battery"] = value


update_stats.pub_time = time.time()
temp_topic = "home/sensors/temperature/+"
power_topic = "home/sensors/mains/mainsPower"
solar_topic = "home/sensors/solar/+"
rh_topic = "home/sensors/humidity/+"

mqtt.subscribe(temp_topic)
mqtt.message_callback_add(temp_topic, on_temp_message)

mqtt.subscribe(power_topic)
mqtt.message_callback_add(power_topic, on_power_message)

mqtt.subscribe(solar_topic)
mqtt.message_callback_add(solar_topic, on_solar_message)

mqtt.subscribe(rh_topic)
mqtt.message_callback_add(rh_topic, on_rh_message)

mqtt.blocking_run()