from threading import Thread
import copy
import logging
from libs.master_clock import MasterClock
from libs.logging_setup import setup_logging
setup_logging()

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)

class FutureRequestProcessor:
    def __init__(self, request_queue):
        self.request_queue = request_queue
        self.handlers = []
        self.thread = Thread(target=self.run)
        self.thread.start()
        self.logger = module_logger.getChild(self.__class__.__name__)
        self.logger.addHandler(logging.NullHandler())

    def run(self):
        while True:
            request = self.request_queue.get()
            self.logger.debug("Got request: {0!s}".format(request))
            for handler in self.handlers:
                outbound_request = handler.handle(request)
                if outbound_request:
                    self.logger.debug("Sending outbound request: {0!s}".format(outbound_request))

    def publish_request(self, request):
        request_json = request.to_json()
        topic = self.request_topic_root+"/"+request.data["zone"]
        self.cache.client.publish_retained(topic, request_json)


class RampHandler:
    def __init__(self, zone, ramp_gradient):
        self.zone = zone
        self.ramp_gradient = ramp_gradient

    def handle(self, request):
        if request.data["zone"] != self.zone:
            return
        if "metadata" not in request.data:
            return # can't compute ramp without target
        metadata = request.data["metadata"]

        if "targetTemp" not in metadata:
            return # can't compute ramp without target
        if "currentTemp" not in metadata:
            return  # can't compute ramp without current temp

        future = request.data["timestamp"]
        current_temp = metadata["currentTemp"]
        target_temp = metadata["targetTemp"]
        now = MasterClock.instance.get_current_time()
        request_gradient = self.compute_ramp(future, now, current_temp, target_temp)
        if request_gradient >= self.ramp_gradient:
            # should start to ramp up now.
            request_target = self.compute_ramp_point_temp(target_temp, now, future)
            metadata["targetTemp"] = request_target
            metadata["future"] = copy.deepcopy(request.data)
            return request
        return None

    def compute_ramp_point_temp(self, target, now, future):
        future_y = target
        future_x = future
        now_x = now
        delta_x = future_x - now_x
        delta_y = (delta_x/3600) * self.ramp_gradient
        now_y = future_y - delta_y
        # TODO Does this assuming heating?
        # Oh fuck off.  I live in Ireland.
        return now_y

    def compute_ramp(self, future, now, current, target):
        delta = target - current
        if delta < 0:
            return # negative shouldn't happen??? unless it's cooling

        time_delta = future - now
        request_gradient = float(delta) / float(time_delta)
        request_gradient_degrees_per_hour = float(delta) / float((time_delta / 3600))

        return request_gradient_degrees_per_hour