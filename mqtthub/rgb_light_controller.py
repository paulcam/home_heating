from libs.mqtt_lib import MqttClient, MqttMultiTopicObservableCache
from libs.master_clock import MasterClock
from libs import sunpos
import time
import datetime
import json
import logging
import threading
from libs.logging_setup import setup_logging
setup_logging()

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)
location = (54.6561998,-5.7052673)

colors = {
    -5: [0,0,32],
     0: [192,32,32],
     5: [255,128, 192],
    10: [255,200,200],
    15: [255,255,255]
}

class RFLightController:
    def __init__(self, client, cache, on_off_topic="RFBridge/relay/3", light_topic="office_overhead", theme_topic="home/lights/themes/standard"):
        self.client:MqttClient = client
        self.cache:MqttMultiTopicObservableCache = cache
        self.on_off_topic = on_off_topic
        self.light_topic = light_topic
        self.cache.add_subscription(self.on_off_topic)
        self.cache.add_observer(self.on_off_topic, self)
        self.theme_topic = theme_topic
        self.cache.add_subscription(theme_topic)
        self.cache.add_observer(self.theme_topic, self)
        self.cache.add_subscription("zigbee2mqtt/{0!s}".format(light_topic))

    def notify(self, topic):
        value = self.cache.last_for_topic(topic)
        if topic == self.on_off_topic:
            module_logger.debug("Valid switch message {0!s} on {1!s}".format(value, topic))
            state = int(value)
            if state == 1:
                self.turnon(self.light_topic)
            else:
                self.turnoff(self.light_topic)
        elif topic == self.theme_topic:
            current_state = self.cache.last_for_topic(self.light_topic)
            if current_state and int(current_state) == 1:
                self.publish_params(self.light_topic)

    def publish_params(self, bulb):
        topic = "zigbee2mqtt/{0!s}/set".format(bulb)
        theme = cache.last_for_topic(self.theme_topic)
        if isinstance(theme, dict):
            module_logger.debug("publishing theme paramters {0!s} to {1!s}".format(theme, bulb))
            payload = json.dumps(theme)
            # validate?
            self.client.publish(topic, payload)
        #elif isinstance(json_str, str):
            #module_logger.debug("Params is string: <{0!s}>".format(json_str))

    def turnon(self, bulb):
        topic = "zigbee2mqtt/{0!s}/set".format(bulb)
        now = datetime.datetime.now()
        payload = json.dumps({
            "state": "ON"
        })
        self.client.publish(topic, payload)

    def turnoff(self, bulb):
        topic = "zigbee2mqtt/{0!s}/set".format(bulb)
        payload = json.dumps({
            "state": "OFF"
        })
        self.client.publish(topic, payload)


class XiaomiButtonController(RFLightController):
    def __init__(self, client, cache, on_off_topic="zigbee2mqtt/office_switch/click", light_topic="office_overhead",
                 theme_topic="home/lights/themes/standard"):
        super().__init__(client, cache, on_off_topic, light_topic, theme_topic)
    def notify(self, topic):
        if topic == self.on_off_topic:
            payload = self.cache.last_for_topic(self.on_off_topic)
            if payload is None or isinstance(payload, int) or len(payload) < 1:
                return
            if topic == self.on_off_topic:
                module_logger.debug("Valid switch message {0!s} on {1!s}".format(payload, topic))
                if payload == "single":
                    self.toggle(self.light_topic)
        elif topic == self.theme_topic:
            module_logger.debug("Theme message on {0!s}".format(topic))
            if self.get_state(self.light_topic) == "ON":
                self.publish_params(self.light_topic)

    def get_state(self, bulb):
        state = "OFF"
        topic = "zigbee2mqtt/{0!s}".format(bulb)
        last_update = self.cache.last_for_topic(topic)
        if isinstance(last_update, dict):
            if "state" in last_update:
                state = last_update["state"]
        return state

    def toggle(self, bulb):
        topic = "zigbee2mqtt/{0!s}/set".format(bulb)
        now = datetime.datetime.now()
        payload = json.dumps({
            "state": "TOGGLE"
        })
        self.client.publish(topic, payload)



class IgnoreMeLightController(RFLightController):
    def __init__(self, client, cache, on_off_topic="RFBridge/relay/3", light_topic="office_overhead", mode_topic="RFBridge/relay/2"):
        super().__init__(client, cache, on_off_topic, light_topic)
        self.mode_topic = mode_topic
        self.client.subscribe(self.mode_topic)
        self.client.message_callback_add(self.mode_topic, self.mode_notify)
        self.mode = "MANUAL"
        self.tracker = None
        self.mode_toggle_state = 0

    def mode_notify(self, client, userdata, message):
        if message.topic == self.mode_topic:
            if int(message.payload) != self.mode_toggle_state:
                module_logger.debug("Valid switch message {0!s} on {1!s}".format(int(message.payload), message.topic))
                self.cycle_mode()
                self.mode_toggle_state = int(message.payload)

    def cycle_mode(self):
        if self.mode == "MANUAL":
            self.mode = "sun_tracker"
            self.start_tracker(self.mode)
        else:
            self.stop_tracker()
            self.reset_to_white("office_overhead")
            self.mode = "MANUAL"

    def sun_tracker(self):
        module_logger.info("Sun tracker thread started.")
        bulb = self.light_topic
        self.trackerStop = False
        while(not self.trackerStop):
            right_now = datetime.datetime.now()
            when = (right_now.year, right_now.month, right_now.day, right_now.hour, right_now.minute,right_now.second, 0 )
            azimuth, elevation = sunpos.sunpos(when, location, True)
            module_logger.info("Sun elevation @ {0!s} = {1!s}".format(right_now, elevation))
            # determine color  or temp
            r, g, b = self.determine_color(elevation)
            # send 1 minute transition?  or just set it hard.
            while(not self.trackerStop and range(60)):
                time.sleep(1)
        module_logger.info("Sun tracker thread ended.")


    def start_tracker(self, mode):
        if self.tracker:
            return
        self.tracker = threading.Thread(target=self.sun_tracker)
        self.tracker.start()

    def stop_tracker(self):
        self.trackerStop = True
        self.tracker.join()
        self.tracker = None

    def reset_to_white(self, bulb):
        topic = "zigbee2mqtt/{0!s}/set".format(bulb)
        payload = json.dumps({
            "hue_power_on_behavior": "on",
            "hue_power_on_brightness": 125,
            "hue_power_on_color_temperature": 280,
        })
        self.client.publish(topic, payload)

    def determine_color(self, elevation):
        global colors
        for elev_deg in sorted(colors.keys()):
            from_elev = elev_deg
            if elevation < elev_deg:
                break

        for elev_deg in sorted(colors.keys()):
            to_elev = elev_deg
            if elevation > elev_deg:
                break

        from_rgb = colors[from_elev]
        to_rgb = colors[to_elev]
        from_to_diff = abs(to_elev-from_elev)


class StandardThemePublisher:
    def __init__(self, client, cache, solar_power_topic="/sensors/panelPower", theme_topic="home/lights/themes/standard"):
        self.client = client
        self.cache = cache
        self.cache.add_subscription(solar_power_topic)
        self.cache.add_observer(solar_power_topic, self)
        self.solar_power_topic = solar_power_topic
        self.theme_topic = theme_topic
        self.params = dict()

    def notify(self, topic):
        self.update_theme_params()
        self.publish_params()

    def update_theme_params(self):
        # Daylight settings
        brightness = 255
        transition = 1
        color_temp = 400

        # Is it dark?
        panel_power = self.cache.last_for_topic(self.solar_power_topic)
        module_logger.debug("Panel Power: {0!s}".format(panel_power))
        if panel_power:
            if "value" in panel_power:
                value = panel_power["value"]
                module_logger.debug("Panel Power Value: {0!s}".format(value))
                if value < 0.2:
                    module_logger.debug("Night time")
                    brightness = 128
                    transition = 3
                    color_temp = 300

        # After midnight?
        now = datetime.datetime.now()
        hour = now.hour
        if hour < 7:
            module_logger.debug("Deep night time")
            brightness = 64
            transition = 10
            color_temp = 400
        self.params["brightness"] = brightness
        self.params["transition"] = transition
        self.params["color_temp"] = color_temp


    def publish_params(self):
        topic = self.theme_topic
        json_body = {
                "brightness": self.params["brightness"],
                "transition": self.params["transition"],
                "color_temp": self.params["color_temp"]
            }

        payload = json.dumps(json_body)
        self.client.publish(topic, payload)


mqtt = MqttClient(broker_address="10.0.0.199", broker_port=1883, client_name="OfficeOHController")
cache = MqttMultiTopicObservableCache(mqtt, [], "value")
stdThemeCtrl = StandardThemePublisher(mqtt, cache)

bah_cntlr = XiaomiButtonController(mqtt, cache)
boh_cntlr = XiaomiButtonController(mqtt, cache, on_off_topic="zigbee2mqtt/bedroom_switch/click", light_topic="bedroom_overhead")
br_light = RFLightController(mqtt, cache , "RFBridge/relay/4", "bedroom_overhead")
mqtt.blocking_run()
