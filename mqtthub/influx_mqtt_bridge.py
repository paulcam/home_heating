import math
import json
import logging
import time
import re
from datetime import datetime

from influxdb import InfluxDBClient

from libs.logging_setup import setup_logging
from libs.master_clock import MasterClock
from libs.mqtt_lib import MqttClient
from libs.value_objects import ExpirableData, TimestampedJSONObject

setup_logging()

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.INFO)
logging.getLogger("urllib3.connectionpool").setLevel(logging.ERROR)
client = InfluxDBClient("10.0.0.199", 8087, 'root', 'root', 'home_auto', pool_size=100)
mqtt = MqttClient(broker_address="10.0.0.199", broker_port=11883, client_name="InfluxBridgeTest")

temp_topic = "home/sensors/temperature/+"
rh_topic = "home/sensors/humidity/+"
sch_topic = "home/heating/schedules/+"
tts_topic = "home/heating/active_targets/+"
control_topic = "home/heating/control/+/+"
presence_topic = "home/presence/+"
solar_topic = "home/sensors/solar/+"
solar_energy_topic = "home/sensors/solar/energy/+"

mains_topic = "home/sensors/mains/+"
power_topic = "home/sensors/power/+"

main_battery_topic = "home/sensors/main-battery/+"
multiplus_topic = "home/sensors/multiplus/#"
mppt2_topic = "home/sensors/solar/mppt2/#"
mppt3_topic = "home/sensors/solar/mppt3/#"

def on_mppt(mqtt_client, userdata, message):
    publish_mppt(userdata, message, "solar")

def on_multiplus(mqtt_client, userdata, message):
    publish_multiplus(userdata, message, "multiplus")

def on_jkbms(mqtt_client, userdata, message):
    publish_jkbms(userdata, message, "main_battery")

def on_temp_message(mqtt_client, userdata, message):
    publish(userdata, message, "temperature")

def on_rh_message(mqtt_client, userdata, message):
    publish(userdata, message, "humidity")


def on_sch_message(mqtt_client, userdata, message):
    publish_sch(userdata, message, "schedules")


def on_tts_message(mqtt_client, userdata, message):
    publish_tts(userdata, message, "active_targets")


def on_control_message(mqtt_client, userdata, message):
    publish_control(userdata, message, "control_states")


def on_presence_message(mqtt_client, userdata, message):
    publish_presence(userdata, message, "presence")


def on_solar_message(mqtt_client, userdata, message):
    publish_solar(userdata, message, "solar")


def on_mains_message(mqtt_client, userdata, message):
    publish_mains(userdata, message, "mains")

def on_power_message(mqtt_client, userdata, message):
    publish_power(userdata, message, "power")


def publish_solar(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    metric = str(topic).rpartition("/")[2]
    obj = ExpirableData.from_json(payload, "solar")
    data = obj.data
    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    #if float(data["value"]) < -20 or float(data["value"]) > 84:
    #    return
    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": data["key"],
                "name": data["name"],
                "units": data["units"]
            },
            "time": timestamp,
            "fields": {
                "value": float(data["value"])
            }
        }
    ]
    write_json(json_body)


def publish_mains(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    topic = message.topic
    metric = str(topic).rpartition("/")[2]
    print("Metric: {}".format(metric))
    obj = ExpirableData.from_json(payload, "mains")
    data = obj.data
    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": data["key"],
                "name": data["name"],
                "units": data["units"]
            },
            "time": timestamp,
            "fields": {
                "value": float(data["value"])
            }
        }
    ]
    write_json(json_body)

def publish_power(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    topic = message.topic
    metric = str(topic).rpartition("/")[2]
    print("Metric: {}".format(metric))
    obj = ExpirableData.from_json(payload, "power")
    data = obj.data
    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": data["key"],
                "name": data["name"],
                "units": data["units"]
            },
            "time": timestamp,
            "fields": {
                "value": float(data["value"])
            }
        }
    ]
    write_json(json_body)



def publish_control(userdata, message, measurement):
    module_logger.debug("publish_control")
    topic = message.topic
    payload = message.payload
    data = json.loads(payload)
    timestamp = data["timestamp"]
    expiry = data["expiry"]
    type = str(topic.split("/")[-2])
    device = str(topic.split("/")[-1])
    now = MasterClock.instance.get_current_time()
    state = str(data["state"])
    dt = datetime.utcfromtimestamp(data["timestamp"])

    if timestamp + expiry < now:
        module_logger.debug("Control State Expired {0!s} for {1!s}".format(expiry, device))
        state = "OFF"
        dt = datetime.utcnow()


    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": device,
                "type": type
            },
            "time": dt,
            "fields": {
                "value": state
            }
        }
    ]
    write_json(json_body)


def write_json(json_body):
    module_logger.debug("Write to influx: {0!s}".format(json_body))
    try:
        client.write_points(json_body)#, retention_policy="one_year")
    except Exception as e:
        print("Error {0!s}".format(e));
        

def publish_tts(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    data = json.loads(payload)
    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    target_zone = str(topic.rpartition("/")[2])

    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": target_zone,
                "schedule": data["schedule"],
                "sympathetic": data["sympathetic"]
            },
            "time": timestamp,
            "fields": {
                "value": float(data["target"])
            }
        }
    ]
    write_json(json_body)


def publish_sch(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    data = json.loads(payload)
    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": str(data["name"]).replace(" ", ""),
                "name": data["name"],
                "targetProfile": data["targetProfile"]
            },
            "time": timestamp,
            "fields": {
                "value": bool(data["active"])
            }
        }
    ]
    write_json(json_body)


def publish_presence(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    data = json.loads(payload)
    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    zone = str(topic.split("/")[-1])
    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": str(zone),
            },
            "time": timestamp,
            "fields": {
                "value": bool(data["isPresent"])
            }
        }
    ]
    write_json(json_body)


def publish_jkbms(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    metric = str(topic).rpartition("/")[2]
    obj = TimestampedJSONObject.from_json(payload, "")
    data = obj.data
    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    if data["units"] != "none":
        value = float(data["value"])
    else:
        return

    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": data["key"],
                "name": data["name"],
                "units": data["units"]
            },
            "time": timestamp,
            "fields": {
                "value": value
            }
        }
    ]
    write_json(json_body)

def publish_multiplus(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    obj = TimestampedJSONObject.from_json(payload, "")
    data = obj.data
    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    if "/ACOut/" in topic:
        data["key"] = "ACOut_"+data["key"]
        data["name"] = "ACOut_"+data["name"]
    elif "/ACActiveIn/" in topic:
        data["key"] = "ACActiveIn_"+data["key"]
        data["name"] = "ACActiveIn_"+data["name"]
    elif "/Energy/" in topic:
        data["key"] = "Energy_"+data["key"]
        data["name"] = "Energy_"+data["name"]
    elif "/DC/" in topic:
        data["key"] = "DC_"+data["key"]
        data["name"] = "DC_"+data["name"]
    else:
        return
    value = float(data["value"])
    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": data["key"],
                "name": data["name"],
                "units": data["units"]
            },
            "time": timestamp,
            "fields": {
                "value": value
            }
        }
    ]
    write_json(json_body)


def publish_mppt(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    obj = TimestampedJSONObject.from_json(payload, "")
    data = obj.data
    zone = str(topic).rpartition("/")[2]
    
    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    value = float(data["value"])
   
    if "mppt2" in message.topic:
        prefix="mppt2_"
    elif "mppt3" in message.topic:
        prefix="mppt3_"
    else:
        prefix="uknown"

    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": prefix+data["key"],
                "name": prefix+data["name"],
                "units": data["units"]
            },
            "time": timestamp,
            "fields": {
                "value": value
            }
        }
    ]
    write_json(json_body)


def publish(userdata, message, measurement):
    topic = message.topic
    payload = message.payload
    zone = str(topic).rpartition("/")[2]
    obj = ExpirableData.from_json(payload, zone)
    data = obj.data
    timestamp = datetime.utcfromtimestamp(data["timestamp"])
    if math.isnan(float(data["value"])):
        module_logger.debug("NaN value received: {0!s}".format(data))
        return
    if measurement=="temperature" and ( float(data["value"]) < -20 or float(data["value"]) > 84):
        module_logger.debug("OutOfWhack value received: {0!s}".format(data))
        return
    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "key": data["key"],
                "name": data["name"]
            },
            "time": timestamp,
            "fields": {
                "value": float(data["value"])
            }
        }
    ]
    write_json(json_body)

def on_mppt_tot(client, userdata, message):
    try:
        obj = TimestampedJSONObject.from_json(message.payload, "")
        data = obj.data
        timestamp = datetime.utcfromtimestamp(data["timestamp"])
        json_body = [
            {
               "measurement": "solar",
                "tags": {
                    "key": data["key"],
                    "name": data["name"]
                },
                "time": timestamp,
                "fields": {
                    "value": float(data["value"])
                }
            }
        ]   
        write_json(json_body)
    except Exception as e:
        module_logger.error("MPPT Total Failed: {0!s} - {1!s}".format(message.payload, e))
        
        pass

master_clock = MasterClock(mqtt, "home/clock")
master_clock.subscribe_to_clock()
time.sleep(1)

mqtt.subscribe(temp_topic)
mqtt.subscribe(rh_topic)
mqtt.subscribe(sch_topic)
mqtt.subscribe(tts_topic)
mqtt.subscribe(control_topic)
mqtt.subscribe(presence_topic)
mqtt.subscribe(solar_topic)
mqtt.subscribe(solar_energy_topic)
mqtt.subscribe(mains_topic)
mqtt.subscribe(power_topic)
mqtt.subscribe(main_battery_topic)
mqtt.subscribe(multiplus_topic)
mqtt.subscribe(mppt2_topic)
mqtt.subscribe(mppt3_topic)
mqtt.subscribe("home/sensors/solar/mppt_total")
mqtt.subscribe("home/sensors/solar/battery_power_limit")

mqtt.message_callback_add(temp_topic, on_temp_message)
mqtt.message_callback_add(rh_topic, on_rh_message)
mqtt.message_callback_add(sch_topic, on_sch_message)
mqtt.message_callback_add(tts_topic, on_tts_message)
# TODO callback update not enough for control graphing.  lack of "OFF" updates
# this only fires when an message is published, so nothing is published with the contro lis OFF
mqtt.message_callback_add(control_topic, on_control_message)
mqtt.message_callback_add(presence_topic, on_presence_message)
mqtt.message_callback_add(solar_topic, on_solar_message)
mqtt.message_callback_add(solar_energy_topic, on_solar_message)
mqtt.message_callback_add(mains_topic, on_mains_message)
mqtt.message_callback_add(power_topic, on_power_message)
mqtt.message_callback_add(main_battery_topic, on_jkbms)
mqtt.message_callback_add(multiplus_topic, on_multiplus)
mqtt.message_callback_add(mppt2_topic, on_mppt)
mqtt.message_callback_add(mppt3_topic, on_mppt)
mqtt.message_callback_add("home/sensors/solar/mppt_total", on_mppt_tot)
mqtt.message_callback_add("home/sensors/solar/battery_power_limit", on_mppt_tot)

mqtt.blocking_run()
