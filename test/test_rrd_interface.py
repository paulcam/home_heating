import sys, os
sys.path = [os.path.abspath(os.path.dirname(__file__))+'../'] + sys.path

from rrd_interface import RRDStorage


class TestRRDInterface:
    def setup(self):
        self.rrdi = RRDStorage()

    def test_getRRDFilename(self):
        result = self.rrdi.get_rrd_filename("foo")
        print("RRDI.test_get_rrd_filename( foo ) {0!s}".format( result))

    def test_RRDExists(self):
        result = self.rrdi.does_rrd_exist("foo")
        print("foo exists? {0!s}".format(result))

    def test_create_rrd(self):
        self.rrdi.create_rrd("DS","'C")

