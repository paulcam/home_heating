import sys
import time
import json
import logging
import hashlib
import os

import socket

PROGRESS = True
# Commands
FLASH = 0

firmwares = {"B4:E6:2D:3A:92:77": "OfficeRadiatorSwitchV1"}

# update_progress() : Displays or updates a console progress bar
## Accepts a float between 0 and 1. Any int will be converted to a float.
## A value under 0 represents a 'halt'.
## A value at 1 or bigger represents 100%
def update_progress(progress):
  if (PROGRESS):
    barLength = 60 # Modify this to change the length of the progress bar
    status = ""
    if isinstance(progress, int):
      progress = float(progress)
    if not isinstance(progress, float):
      progress = 0
      status = "error: progress var must be float\r\n"
    if progress < 0:
      progress = 0
      status = "Halt...\r\n"
    if progress >= 1:
      progress = 1
      status = "Done...\r\n"
    block = int(round(barLength*progress))
    text = "\rUploading: [{0}] {1}% {2}".format( "="*block + " "*(barLength-block), int(progress*100), status)
    sys.stderr.write(text)
    sys.stderr.flush()
  else:
    sys.stderr.write('.')
    sys.stderr.flush()


def serve(remoteAddr, localAddr, remotePort, localPort, password, filename, command=FLASH):
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (localAddr, localPort)
    logging.info('Starting on %s:%s', str(server_address[0]), str(server_address[1]))
    try:
        sock.bind(server_address)
        sock.listen(1)
    except Exception as e:
        logging.error("Listen Failed {0!s}".format(e))
        return 1

    content_size = os.path.getsize(filename)
    f = open(filename, 'rb')
    file_md5 = hashlib.md5(f.read()).hexdigest()
    f.close()
    logging.info('Upload size: %d', content_size)
    message = '%d %d %d %s\n' % (command, localPort, content_size, file_md5)

    # Wait for a connection
    logging.info('Sending invitation to: %s', remoteAddr)
    sock2 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    remote_address = (remoteAddr, int(remotePort))
    sent = sock2.sendto(message.encode(), remote_address)
    sock2.settimeout(10)
    try:
        data = sock2.recv(128).decode()
    except:
        logging.error('No Answer')
        sock2.close()
        return 1
    if (data != "OK"):
        logging.error('Bad Answer: %s', data)
        sock2.close()
        return 1
    sock2.close()

    logging.info('Waiting for device...')
    try:
        sock.settimeout(60)
        connection, client_address = sock.accept()
        sock.settimeout(None)
        connection.settimeout(None)
    except:
        logging.error('No response from device')
        sock.close()
        return 1

    received_ok = False

    try:
        f = open(filename, "rb")
        if (PROGRESS):
            update_progress(0)
        else:
            sys.stderr.write('Uploading')
            sys.stderr.flush()
        offset = 0
        while True:
            chunk = f.read(1460)
            if not chunk: break
            offset += len(chunk)
            update_progress(offset / float(content_size))
            connection.settimeout(10)
            try:
                connection.sendall(chunk)
                if connection.recv(32).decode().find('O') >= 0:
                    # connection will receive only digits or 'OK'
                    received_ok = True
            except:
                sys.stderr.write('\n')
                logging.error('Error Uploading')
                connection.close()
                f.close()
                sock.close()
                return 1

        sys.stderr.write('\n')
        logging.info('Waiting for result...')
        # libraries/ArduinoOTA/ArduinoOTA.cpp L311 L320
        # only sends digits or 'OK'. We must not not close
        # the connection before receiving the 'O' of 'OK'
        try:
            connection.settimeout(60)
            received_ok = False
            received_error = False
            while not (received_ok or received_error):
                reply = connection.recv(64).decode()
                # Look for either the "E" in ERROR or the "O" in OK response
                # Check for "E" first, since both strings contain "O"
                if reply.find('E') >= 0:
                    sys.stderr.write('\n')
                    logging.error('%s', reply)
                    received_error = True
                elif reply.find('O') >= 0:
                    logging.info('Result: OK')
                    received_ok = True
            connection.close()
            f.close()
            sock.close()
            if received_ok:
                return 0
            return 1
        except:
            logging.error('No Result!')
            connection.close()
            f.close()
            sock.close()
            return 1

    finally:
        connection.close()
        f.close()
# end serve

s=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(('',8267))

while( True ):
    m=s.recvfrom(1024)
    data = m[0]
    json_str = data.decode("utf-8")
    espDevice = json.loads(json_str)

    print("MAC: {}".format(espDevice["macAddress"]))
    print("FW:  {}".format(espDevice["firmware"]))
    print("IP:  {}".format(espDevice["ipAddress"]))

    if espDevice["macAddress"] in firmwares:
        print("Device is known")
        macAddress=  espDevice["macAddress"]
        currentFw = espDevice["firmware"]
        requiredFw = firmwares[macAddress]

        #if currentFw != requiredFw:
        print("Device is running {} and should be running {}".format(currentFw, requiredFw))
        print("Flashing...")
        ret = serve( espDevice["ipAddress"], "10.0.0.5", 8266, 8266, "", requiredFw )
        print("Flashed ret code {}".format(ret))
        #else:
        #    print("Device is running {} OK".format(currentFw))

    time.sleep(10)

