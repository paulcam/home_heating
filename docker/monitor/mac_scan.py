#!/usr/bin/env python3
import pypureomapi
import time
import re
import sys
from pprint import pprint
#from mac_vendor_lookup import MacLookup
from telegraf_pyplug.main import print_influxdb_format
from pythonping import ping, executor
from pythonping.executor import Response, ResponseList
import threading

KEYNAME = b"defomapi"
BASE64_ENCODED_KEY = b"+bFQtBCta6j2vWkjPkNFtgA=="  # FIXME: be sure to replace this by your own key!!!

dhcp_server_ip = "10.0.0.199"
port = 7911  # Port of the omapi service

omapi = pypureomapi.Omapi(dhcp_server_ip, port, KEYNAME, BASE64_ENCODED_KEY)
regex = re.compile("hardware ethernet (.*);")

known_macs =  set()
mac_data = dict()
ping_data = dict()
interesting_fields = ["state", "client-hostname", "ip-address", "ddns-fwd-name", "hardware-address" ]
with open("/home/paul/devel/docker/network-services/dhcpd-conf/dhcpd.leases") as f:
    for line in f:
        result = regex.search(line)
        if result:
            known_macs.add(result.group(1))


def ompai_scan():
    for mac in known_macs:
        mac_data[mac] = dict()
        try:
            lease = omapi.lookup_by_lease(mac=mac)
            for k, v in lease.items():
                if k in interesting_fields:
                    if isinstance(v, (bytes, bytearray)):
                        mac_data[mac][k] = str(v, "UTF-8" )
                    else:
                        mac_data[mac][k] = str(v)

            if lease["ends"] < time.time() or lease["state"] != 2:
                mac_data[mac]["valid"] = True
            else:
                mac_data[mac]["valid"] = False
            try:
                pass
                #mac_data[mac]["vendor"] = MacLookup().lookup(mac)
            except KeyError as e:
                mac_data[mac]["vendor"] = "UNKNOWN"

        except pypureomapi.OmapiErrorNotFound:
            mac_data[mac]["error"] = "Not Found"

        except Exception as e:
            print("Error {0!s}".format(e))
            sys.exit(1)


def ping_host(ip:str):
    responses:ResponseList = ping(ip, timeout=1, count=1)
    if responses:
        data= {
            "packet_loss": responses.packet_loss,
            "rtt_min": responses.rtt_min_ms,
            "success": responses.packet_loss == 0
        }
        ping_data[ip] = data


def ping_scan():
    threads = [] # really cool movie too
    for key, mac in mac_data.items():
        if mac and "ip-address" in mac:
            ip = mac["ip-address"]
            if ip:
                threads.append(threading.Thread(target=ping_host, kwargs={"ip": ip}))
    for t in threads:
        t.start()
    for t in threads:
        t.join()


def print_macs_as_influx():
    for mac in mac_data:
        fields = {key: value for key, value in mac.items() if key in [""]}
        tags = {key: value for key, value in mac.items() if key not in [""]}

        print_influxdb_format(
            measurement='dhcp_status',
            fields={
                'field_float': 1,
                'field_int': '123i',
                'field_str': 'two',
                'field_bool': True,
            }
        )

ompai_scan()
ping_scan()
pprint(mac_data)
pprint(ping_data)
