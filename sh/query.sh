#!/bin/bash

data=$(curl -s 10.0.0.3:9998)

IFS=',' read -ra DATA <<< "$data"
for i in "${DATA[@]}"; do
    # process "$i"
    IFS=':' read -ra DATUM <<< "$i"
    echo -e "${DATUM[0]}:\t${DATUM[1]} ${DATUM[2]}"
done
