import requests
import json
from pyspark.sql import SparkSession

#SELECT count("value") FROM "one_day"."control_states" WHERE ("type" = 'radiator') AND time >= now() - 24h GROUP BY time(1m), "key" fill(none)

db = "http://10.0.0.3:8087/query?pretty=true"
config = { "db":"home_auto",
           "q":
"""SELECT count("value") FROM "one_day"."control_states" WHERE ("type" = 'radiator') AND time >= now() - 24h GROUP BY time(1m), "key" fill(none)"""
           }
response = requests.get(db, params=config)

json_str = response.json()
ss = SparkSession.builder.master("local[4]").getOrCreate()
sc = ss.sparkContext

raw_data = ss.read.json(sc.parallelize(json_str))
raw_data.show()