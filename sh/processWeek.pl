#!/usr/bin/perl
#
use Date::Calc qw(Day_of_Week_to_Text);
use Date::Calc qw(Day_of_Week);
use Time::Local;
use Date::Calc qw(Add_Delta_Days);

(($#ARGV + 1) == 8) or die("Usage: processWeek.pl rrd_file start_day start_mon start_year number_of_days name key unit");

$rrd_file = $ARGV[0];
$startDay = $ARGV[1];
$startMon = $ARGV[2];
$startYear = $ARGV[3];
$numberOfDays = $ARGV[4];
$name = $ARGV[5];
$key = $ARGV[6];
$unit = $ARGV[7];

$baseCommand = "rrdtool graph ";
$startDate = timelocal( 0,0,0, $startDay, $startMon-1, $startYear );
$startDOW = Day_of_Week( $startYear, $startMon, $startDay );
$days = 0;
$date = $startDate;
do
{	
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($date);
	if( $wday == 0 )
	{		
		$wday = 7;
	}
	$dayText = Day_of_Week_to_Text($wday);

	$from = "Midnight ".($mon+1)."/$mday/".($year+1900);


	# Label times
	$awake = timelocal(0,0,7,$mday,$mon,$year);
	$outToWork = timelocal(0,0,8,$mday,$mon,$year);
	$inFromWork = timelocal(0,45,17,$mday,$mon,$year);
	$bed = timelocal(0,30,22,$mday,$mon,$year);


	$date = $date + (60*60*24);
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($date);
	$to = "Midnight ".($mon+1)."/$mday/".($year+1900);


	$command = $baseCommand."\"images/".$name."_".($mday-1)."_".($mon+1)."_".($year+1900).".png\" ";
	$command .= "--start \"$from\" ";
	$command .= "--end \"$to\" ";
	$command .= "--imgformat PNG --width 1200 --height 400 ";
	$command .= "--title \"".$name." ".($mday-1)."/".($mon+1)."/".($year+1900)."\" --vertical-label '$unit' --left-axis-format %.1lf ";
	$command .= "DEF:".$name."=".$rrd_file.":$key:MAX ";
	$command .= "AREA:".$name."#0000FF:\"$key\" ";
	$command .= "GPRINT:".$name.":AVERAGE:\"Avg\\: %5.2lf\" ";
	$command .= "GPRINT:".$name.":MAX:\"Max\\: %5.2lf\" ";
	$command .= "GPRINT:".$name.":MIN:\"Min\\: %5.2lf\" ";
	if( $wday > 1 && $wday < 7 )
	{
		$command .= "VRULE:$awake#00FF00:\"Awake\" ";
		$command .= "VRULE:$outToWork#FF0000:\"Out to work\" ";
		$command .= "VRULE:$inFromWork#00FF00:\"In from work\" ";
		$command .= "VRULE:$bed#FF0000:\"Bed\" ";
	}

	print "$command\n";
	system( $command );
	$days++;
}
while( $days < $numberOfDays )

