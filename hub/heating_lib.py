import ring_buffer as rb
import threading
import datetime
import time
import socket
from datum import *
import traceback

class DataDAO():
    def __init__(self, hub_ip="10.0.0.3", data_read_port=9998, data_write_port=9999):
        self.hub_ip = hub_ip
        self.data_read_port = data_read_port
        self.data_write_port = data_write_port

    def get_all_data(self):
        try:
            sock = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
            sock.connect((self.hub_ip, self.data_read_port))
            data_str=""
            data_part = sock.recv(65535).decode("utf-8")
            while data_part:
                data_str+=data_part
                data_part = sock.recv(65535).decode("utf-8")

            print("Data: "+data_str)
            datums = Datum.jsonToDict(data_str)
            return datums
        except Exception as e:
            print("Exception while getting data: {0!s}".format(e))
            traceback.print_exc()
            return []

    def get_key(self, key):
        data = self.get_all_data()
        if key in data:
            return data[key]
        else:
            return None

    def publish_data(self, datum):
        datumEncoder = DatumEncoder()
        datum_json = datumEncoder.encode(datum)
        datum_json = "{{\"{0!s}\": {1!s}}}".format(datum.key, datum_json)
        print("Sending JSON datum to {0!s}: {1!s}".format(self.hub_ip,datum_json))
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
        sock.sendto(bytes(datum_json, "utf-8"), (self.hub_ip, self.data_write_port))


class DemandDAO():
    def __init__(self, hub_ip="10.0.0.3", data_read_port=9996, data_write_port=9997):
        self.hub_ip = hub_ip
        self.data_read_port = data_read_port
        self.data_write_port = data_write_port

    def get_all_data(self):
        try:
            sock = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
            sock.connect((self.hub_ip, self.data_read_port))
            data_str = sock.recv(65535).decode("utf-8")
            demands = Demand.jsonToDict(data_str)
            return demands
        except Exception as e:
            print("Exception while getting data: {0!s}".format(e))
            traceback.print_exc()
            return []

    def get_key(self, key):
        data = self.get_all_data()
        if key in data:
            return data[key]
        else:
            return None

    def publish_data(self, datum):
        demandEncoder = DemandEncoder()
        datum_json = demandEncoder.encode(datum)
        datum_json = "{{\"{0!s}\": {1!s}}}".format(datum.key, datum_json)
        print("Sending JSON demand to {0!s}: {1!s}".format(self.hub_ip,datum_json))
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
        sock.sendto(bytes(datum_json, "utf-8"), (self.hub_ip, self.data_write_port))


class HistoryStore(threading.Thread):
    def __init__(self):
        super(HistoryStore,self).__init__()
        self.indoor_1hour = rb.RingBuffer(60)
        self.outdoor_1hour = rb.RingBuffer(60)
        self.indoor_24hour = rb.RingBuffer(12*24)
        self.outdoor_24hour = rb.RingBuffer(12*24)
        self.running = True
        self.indoor_1hour_update = datetime.datetime.utcnow() - datetime.timedelta(minutes=1) 
        self.indoor_24hour_update = datetime.datetime.utcnow() - datetime.timedelta(minutes=5) 
        self.indoor_current = 0
        self.outdoor_current = 0

    def set_current( self, indoor, outdoor ):
        self.indoor_current = indoor
        self.outdoor_current = outdoor

    def stop(self):
        self.running = False

    def run(self):
        while self.running:
            rightnow = datetime.datetime.utcnow()
            if rightnow.second == 0 and (rightnow - datetime.timedelta(minutes=1)) >  self.indoor_1hour_update:
                self.indoor_1hour.append(self.indoor_current)
                self.indoor_1hour.append(self.outdoor_current)                
                self.indoor_1hour_update = rightnow

                if rightnow.minute % 5 == 0 and (rightnow - datetime.timedelta(minutes=5)) >  self.indoor_24hour_update:
                    self.indoor_24hour.append(self.indoor_current)
                    self.indoor_24hour.append(self.outdoor_current)
                    self.indoor_24hour_update = rightnow
            time.sleep(0.1)
        return


