import socket
import requests
from datum import Demand
import time
import traceback
from heating_lib import DataDAO, DemandDAO
from datum import Datum
import re

state = False

class Radiator:
    # should we make zone(s) a list?
    def __init__(self, ncno, zone, ip):
        self.ncno = ncno
        self.zone = zone
        self.ip = ip

    def open(self):
        if self.ncno == "NC":
            self.commandRadiator(True)
        else:
            self.commandRadiator(False)

    def close(self):
        if self.ncno == "NO":
            self.commandRadiator(True)
        else:
            self.commandRadiator(False)

    def off(self):
        self.commandRadiator(False)

    def commandRadiator(self, state):
        radiatorState = self.radiatorStatus()
        print("Commanding radiator {} to state {} current state {}".format(self.zone, state, radiatorState))
        command = "off"
        if state:
            command = "on"

        if radiatorState is None or radiatorState.lower() != command:
            # Launch all threads
            uri = "http://{0!s}/{1!s}".format(self.ip, command)
            print("Commanding radiator {0!s} {1!s} ({2!s})".format(self.zone, command, uri))
            response = send_http_request(uri)
            print(str(response))
        else:
            print("Radiator {} already in state {}".format(self.zone, state))

    def radiatorStatus(self):
        uri = "http://{0!s}/status".format(self.ip)
        response = send_http_request(uri)
        if response is None:
            return None
        status = re.findall(r'ON|OFF', response.text)
        return status[0]


def send_http_request(uri):
    try:
        response = requests.get(uri, verify=False, timeout=10)
        print( response )
    except Exception as e:
        print( "Failed to send command to {0!s} - {1!s}".format(uri, e))
        return None
    return response

def get_heating_status():
    command = "status"

    # Launch all threads
    uri = "http://10.0.0.9/{0!s}".format(command)
    print("Getting heating status {0!s} {1!s}".format(command, uri))
    response = send_http_request(uri)
    status = re.findall(r'ON|OFF', response.text)
    print("Heating status: "+str(status));
    return status[0]

def commandHeating(demandState):

    command = "off"
    if demandState:
        command = "on"

    thread_list = []
    status = get_heating_status();
    if status.lower() != command:
        # Launch all threads
        uri = "http://10.0.0.9/{0!s}".format(command)
        print("Commanding heating {0!s} {1!s}".format(command, uri))
        response = send_http_request(uri)
        print(str(response))
    else:
        print("Heating already in correct status")

    dataDao = DataDAO()
    value = 1 if command == "on" else 0
    dataDao.publish_data(Datum("heating","Heating", "HT", value, "bool", " ", 0))


def gas_boiler_control(demands):
    demandState = False
    for demand in demands.values():
        if demand.category == "CONTROL" and demand.key == "heating":
            demandExpiry = demand.expiry
            timestamp = demand.timestamp
            print("Heating demandState {0!s} demandExpiry {1!s} timestamp {2!s} currentTime {3!s}"
                  .format(demand.demandState, demandExpiry, timestamp, time.time()))
            if (int(timestamp) + int(demandExpiry)) < time.time():
                print("Demand expired.  Setting heating OFF")
                demandState = False
            else:
                if demand.demandState == "ON":
                    demandState = True
                else:
                    demandState = False

                print("Demand state {0!s} current state ".format(demandState))
    commandHeating(demandState)
    return demandState

def radiator_control(demands, radiators, boilerState):
    activeZones = []
    if not boilerState:
        print("Boiler is not on, turning all valves power off.")
        for radiatorKey in radiators:
            radiator = radiators[radiatorKey]
            radiator.off()
        return

    for demandKey in demands:
        demand = demands[demandKey]
        if demand.key.startswith("heating-"):
            parts = demand.key.split("-")
            zone = parts[1]
            if demand.is_expired(time.time()):
                continue # no op
            if demand.demandState =="ON":
                activeZones.append(zone)
    for zone in activeZones:
        zone = zone[0].lower() + zone[1:]
    for radiatorKey in radiators:
        radiator = radiators[radiatorKey]
        if radiator.zone in activeZones:
            radiator.open()
        else:
            radiator.close()


if __name__ == "__main__":
    rads = {}
    rads["office"] = Radiator("NC", "office", "10.0.0.23")
    rads["livingRoom"] = Radiator("NC", "livingRoom", "10.0.0.21")
    rads["bedroom"] = Radiator("NC", "bedroom", "10.0.0.22")

    while True:
        try:
            sock = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
            sock.connect(("10.0.0.3", 9996))
            data = sock.recv(2048).decode("utf-8")

            demandDao = DemandDAO()
            demands = demandDao.get_all_data()
            boilerState = gas_boiler_control(demands)
            radiator_control(demands, rads, boilerState)

        except Exception as e:
            print("Exception in main loop: {0!s}".format(e))
            traceback.print_exc()
        # If an exception is thrown this will hot spin
        time.sleep(30)
