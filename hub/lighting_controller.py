import socket
import requests
from datum import Demand
import time
from threading import Thread

state = False

def send_http_request(uri):
    try:
        response = requests.get(uri, verify=False, timeout=10)
        print( response )
    except Exception as e:
        print( "Failed to send command to {0!s} - {1!s}".format(uri, e))


def commandGroup(demandState):
    command = "off"
    if demandState:
        command = "on"
    print("Commanding group {0!s}".format(command))
    thread_list = []

    # Launch all threads
    for switch in [70, 71, 72]:
        uri = "http://10.0.0.{0!s}/{1!s}".format(str(switch), command)
        thread = Thread(target=send_http_request, args=(uri,))
        thread.start()
        thread_list.append(thread)

    # # Now wait on them joining back up
    # for thread in thread_list:
    #     thread.join()

if __name__ == "__main__":
    while True:
        try:
            sock = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
            sock.connect(("10.0.0.3", 9996))
            data = sock.recv(2048).decode("utf-8")
#            print(data)
            demands = Demand.jsonToDict(data)

            if "groupLight1" in demands:
                demandState = demands["groupLight1"].demandState
                if demandState == "ON":
                    demandState = True
                else:
                    demandState = False

#                print("Demand state {0!s} current state {1!s}".format(demandState, state))
                if demandState != state:
                    commandGroup(demandState)
                    state = demandState
        except Exception as e:
            print("Exception in main loop: {0!s}".format(e))
        time.sleep(1)
