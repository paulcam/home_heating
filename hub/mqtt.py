import math
import paho.mqtt.client as mqtt
import re
import json 
import socket
from datum import Datum
from heating_lib import DataDAO

mains_power_factor = 1
mains_power = 0
mains_rctv = 0

class Payload:
    def __init__(self,data):
        self.__dict__ = json.loads(data)

def publish_power_factor():
    ap = math.sqrt(pow(mains_power, 2) + pow(mains_rctv, 2))
    pf = mains_power / ap
    type = "float"
    units = ""
    key = "mainsPowerFactor"
    name = "Mains Power Factor"
    short_name = "MF"
    datum = Datum(key, name, short_name, float(pf), type, units, 0 )
    print("Publishing datum: {}".format(str(datum)))
    dataDao = DataDAO()
    dataDao.publish_data(datum)





def handle_power(message):
    global mains_power, mains_power_factor, mains_rctv
    print("Power message: {}".format(float(message.payload)))
    topic = message.topic
    match = re.search("shellies/power/emeter/0/(.*)", topic)
    metric = match.group(1)
    print("Metric: {}".format(metric))
    type = "float"
    if metric == "power":
        units = "W"
        key = "mainsPower"
        name = "Mains Power"
        short_name = "MP"
        mains_power = float(message.payload)
        publish_power_factor()

    elif metric == "voltage":
        units = "V"
        key = "mainsVoltage"
        name = "Mains Voltage"
        short_name = "MV"

    elif metric == "total":
        units = "Wh"
        key = "mainsTotal"
        name = "Mains Total"
        short_name = "MT"

    elif metric == "reactive_power":
        units = "W"
        key = "mainsReactive"
        name = "Mains Reactive Power"
        short_name = "MR"
        mains_rctv = float(message.payload)
        publish_power_factor()

    else:
        return
    datum = Datum(key, name, short_name, float(message.payload), type, units, 0 )
    print("Publishing datum: {}".format(str(datum)))
    dataDao = DataDAO()
    dataDao.publish_data(datum)

def on_message(client, userdata, message):
    print( "On message "+message.topic )
    if message.topic.startswith("shellies/power/emeter/0/"):
        handle_power(message)
        return
    payload = int(message.payload)
    print("Received payload "+str(payload))
    status = "OFF"
    if payload == 1:
        status = "ON"
    json_str = '{"groupLight1": {"category": "lights", "expiry": "60000", "timestamp": 1568324357.55425, "key": "groupLight1", "demandState": "'+status+'"}}'
    print("Sending demand: "+json_str)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
    sock.sendto(bytes(json_str, "utf-8"), ("10.0.0.3", 9997))

def on_disconnect(client, userdata, rc):
    if rc != 0:
        print( "Unexpected MQTT disconnection. Will auto-reconnect" )

def on_connect(client,userdata, flags, rc):
    print("Connected.  Subscribing.")
    client.subscribe("RFBridge/relay/0")
    client.subscribe("shellies/power/emeter/0/#")

broker_address="10.0.0.199" 
client = mqtt.Client("Hubtest") #create new instance

client.on_message=on_message        #attach function to callback
client.on_disconnect=on_disconnect
client.on_connect=on_connect

client.connect(broker_address) #connect to broker

client.loop_forever()

