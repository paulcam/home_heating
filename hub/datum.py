import json
from collections import namedtuple
import time

class Datum:
    def __init__(self, key, name, shortName, value, type, units, timestamp):
        self.key = key
        self.name = name
        self.shortName = shortName
        self.value = value
        self.type = type
        self.units = units
        self.timestamp = timestamp

    @staticmethod
    def _from_json(json):
        datum = Datum.jsonToDict(json)
        return datum

    @staticmethod
    def _json_object_hook(d):
        return namedtuple('Dict', d.keys())(*d.values())

    @staticmethod
    def jsonToDict(data):
        ts = json.loads(data, object_hook=Datum._json_object_hook)
        datums = {}
        for t in ts:
            if "timestamp" in t._fields:
                timestamp = t.timestamp
            else:
                timestamp = "0"

            datum = Datum(t.key, t.name, t.shortName, t.value, t.type, t.units, timestamp )
            datums[t.key] = datum

        return datums


class DatumEncoder(json.JSONEncoder):
    def default(self,datum):
        return datum.__dict__


class Demand:
    def __init__(self, key, demandState,  expiry, category="NONE", timestamp=time.time()):
        self.key = key
        self.demandState = demandState
        self.timestamp = timestamp
        self.expiry = expiry
        self.category = category

    def __str__(self):
        return self.category+" "+self.key+" "+str(self.demandState)+" "+str(self.timestamp)+" "+str(self.expiry)+" Expired: "+str(self.is_expired(time.time()))

    def __repr__(self):
        return self.__str__()

    def is_expired(self, timenow):
        return (self.timestamp + float(self.expiry)) < float(timenow)

    @staticmethod
    def _from_json(json):
        demand = Demand.jsonToDict(json)
        return demand

    @staticmethod
    def _json_object_hook(d):
        return namedtuple('Dict', d.keys())(*d.values())

    @staticmethod
    def jsonToDict(data):
        ts = json.loads(data)
        demands = {}
        for demandKey in ts:
            demand = ts[demandKey]

            if "timestamp" not in demand:
                demand["timestamp"] = 0

            if "demandState" not in demand:
                print("Error, demandState is required.")
                continue
            if "expiry" not in demand:
                demand["expiry"] = 300 # default to 5 minute expiry

            if "category" not in demand:
                demand["category"] = "NONE"

            demandObj = Demand(demand["key"], demand["demandState"],  demand["expiry"], demand["category"], demand["timestamp"])
            demands[demandKey] = demandObj

        return demands

class DemandEncoder(json.JSONEncoder):
    def default(self,demand):
        return demand.__dict__
