import RPi.GPIO as GPIO
import time
import datetime
from lcd_lib import *
from heating_lib import HistoryStore
import socket
from datum import Datum, DatumEncoder


hub_ip = "10.0.0.3"
hub_port = 9998
degrees = "'" #u'\N{DEGREE SIGN}'

black_list = ["bI", "bP", "pI", "pV", "lV", "lI", "AT", "P1", "HO", "HI" ]

def centigrade_to_fahrenheit( temp ):
    return temp * (9.0/5.0) + 32.0

def current_temp_screen( data ):
    line = LCD_LINE_1
    for key,datum in sorted(data.items()):
        valueStr = datum.value
        dataType = datum.type
        if dataType == 'float':
            valueFloat = float(valueStr)
            value = "{0:5.2f}".format(valueFloat)
        elif dataType == 'string':
            value = str(valueStr)
        else:
            value = str(valueStr)

        unit = datum.units
        key = datum.shortName
        if key in black_list:
            continue
        currentTime = time.time()
        timestamp = datum.timestamp
        if (currentTime - timestamp) > 400:
            key = "*"+key
        if key == "HT":
            value = "ON" if value == "1" else "OFF"
        if key == "PR":
            value = "HOME" if value == "1" else "AWAY"
        print("Data item: {0!s} {1!s} {2!s}".format(key, value, unit))

        display_str = "{0!s}:{1!s}{2:s}".format(key, value, unit)
        lcd_string("{0:s}".format(display_str), line)
        if line == LCD_LINE_1:
            line = LCD_LINE_2
        else:
            time.sleep(3)
            line = LCD_LINE_1


def get_temps():
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(1)
        sock.connect((hub_ip, hub_port))
        response = sock.recv(4096)
        str_response = response.decode("utf-8")
        datums = Datum.jsonToDict(str_response)
        return datums
    except Exception as e:
        print("Hub appears to be unavailable: {}".format(e))
        return datums
    
    finally:
        sock.close()
        
global history_store
history_store = HistoryStore()

def main():
    # Initialise display
    lcd_init()

    while True:
        temps = get_temps()
        current_temp_screen( temps )
        time.sleep(3)



if __name__ == '__main__':

  try:
    main()
  except KeyboardInterrupt:
    pass
  finally:
    history_store.stop()
    lcd_byte(0x01, LCD_CMD)
    lcd_string("Goodbye!",LCD_LINE_1)
    GPIO.cleanup()
