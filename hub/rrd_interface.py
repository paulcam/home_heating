import os.path
import calendar
import time

RRD_PATH="/code/data/"

class RRDStorage:
    def __init__(self):
        self.rrdFile = None

    def store_now(self, key, value, unit ):
        exists = self.does_rrd_exist(key)
        if not exists:
            self.create_rrd(key, unit)
        self.write_to_rrd(key, value)

    def does_rrd_exist(self, key):
        filename = self.get_rrd_filename(key)
        if( os.path.isfile(filename) ):
            return True
        return False

    def get_rrd_filename(self, key):
        return "{1!s}data_{0!s}.rrd".format(key, RRD_PATH)

    def get_now_epoc(self):
        return calendar.timegm(time.gmtime())

    def write_to_rrd(self, key, value):
        filename = self.get_rrd_filename(key)
        if not value:
            value = 'U'
        command = "rrdtool update {0!s} N:{1!s}".format(filename, value)
        os.system(command)


    def create_rrd(self, key, unit):
        filename = self.get_rrd_filename(key)
        max_min = "0:100"
        if( 'Hpa' in unit ):
            max_min = "800:1200"
        if( unit.endswith('C') ):
            max_min = "-20:100"
        if( unit == "%" ):
            max_min = "0:100"

        now = self.get_now_epoc()

        command = "rrdtool create  {0!s} \
                --step 60 \
                --start {1!s} \
                DS:{2!s}:GAUGE:300:{3!s} \
                RRA:AVERAGE:0.75:1:525600 \
                RRA:AVERAGE:0.75:60:87600".format(filename, now, key, max_min)
        print("RRD Create: {0!s}".format(command))
        output = os.system(command)
        print("Output: {0!s}".format(output))

