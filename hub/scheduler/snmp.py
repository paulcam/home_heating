from pysnmp.entity.rfc3413.oneliner import cmdgen

class SnmpPresence():
    def __init__(self, ip='10.0.0.1', community='public', mib=(1,3,6,1,2,1,4,22,1,2,16)):
        self.ip=ip
        self.community=community
        self.mib=mib

    def isMacOnline(self, mac_address):
        generator = cmdgen.CommandGenerator()
        comm_data = cmdgen.CommunityData('server', self.community, 1) # 1 means version SNMP v2c
        transport = cmdgen.UdpTransportTarget((self.ip, 161))

        real_fun = getattr(generator, 'nextCmd')
        res = (errorIndication, errorStatus, errorIndex, varBinds)\
            = real_fun(comm_data, transport, self.mib)

        if not errorIndication is None  or errorStatus is True:
               print("Error: %s %s %s %s" % res)
               return False
        else:
               for varBind in varBinds:
                   name, valueT = varBind[0]
                   thisMac = valueT.prettyPrint()
                   if thisMac == mac_address:
                       return True