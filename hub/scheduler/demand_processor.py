import socket
import time
import traceback

from datum import Demand


class DemandProcessor():
    def processDemands(self, demands):
        if len(demands) < 1:
            return
        # heating demands
        heatingDemands = {key: value for (key, value) in demands.items() if value.category == "HEATING"}
        heatingDemands = {key: value for (key, value) in heatingDemands.items() if not self.demandExpired(value)}

        print("Active heating demand " + str(heatingDemands))

        if len(heatingDemands) > 0:
            self.processHeatingDemands(heatingDemands)

    def processHeatingDemands(self, heatDemands):
        if len(heatDemands) < 1:
            return

        output_demand = Demand("heating", "ON", 300, "CONTROL") # 10 second minimum

        # Get existing control demands
        existing_demands = self.get_demands()
        existing_demands = {key: value for (key, value) in existing_demands.items() if value.category == "CONTROL"}
        existing_demands = {key: value for (key, value) in existing_demands.items() if not self.demandExpired(value)}


        if "heating" in existing_demands:
            existing_heating_demand = existing_demands["heating"]
            print( "Existing heating demand "+str(existing_heating_demand))
            if self.demandExpired(existing_heating_demand):
                print("Existing heating demand " + str(existing_heating_demand) + " Expired Raising New.")
                self.raiseHeatingDemand(output_demand)
                return
            # will the output_demand set a longer expiry?
            if self.isLonger(existing_heating_demand, output_demand):
                print("Existing heating demand " + str(existing_heating_demand) + " expires before new, raising.")
                self.raiseHeatingDemand(output_demand)
                return
            print("No action.")
        else: # No existing demand, raise a new one
            print("No existing heating demand raising new one. " + str(output_demand))
            self.raiseHeatingDemand(output_demand)
            return

    # Return true is the second demand would last beyond the first
    def isLonger(self, first, second):
        first_expiry_ts = first.timestamp + int(first.expiry)
        second_expiry_ts = second.timestamp + int(second.expiry)
        return first_expiry_ts < second_expiry_ts

    def raiseHeatingDemand(self, demand):
        self.raiseAnyDemand(demand, "heating")

    def raiseAnyDemand(self, demand, key):
        demandJson = "{{\"{3!s}\": {{\"key\":\"{3!s}\", \"demandState\":\"{0!s}\", \"expiry\":\"{1!s}\", " \
                     "\"category\": \"{2!s}\"}}}}".format(demand.demandState, str(demand.expiry), str(demand.category),
                                                          str(key))
        print("Raising demand: " + demandJson)
        self.raiseDemand(demandJson)

    def raiseDemand(self,demandJson):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
        sock.sendto(bytes(demandJson, "utf-8"), ("10.0.0.3", 9997))

    def demandExpired(self, demand):
        currentTime = time.time()
        demandTime = demand.timestamp
        expiry = demand.expiry
        if int(demandTime)+int(expiry) < currentTime:
            return True
        return False

    def get_demands(self):
        try:
            sock = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
            sock.connect(("10.0.0.3", 9996))
            data_str = sock.recv(4096).decode("utf-8")

            datums = Demand.jsonToDict(data_str)
            return datums

        except Exception as e:
            print("Exception in main loop: {0!s}".format(e))
            traceback.print_exc()
            return []

def main():
    demandProcessor = DemandProcessor()
    while True:
        demands = demandProcessor.get_demands()
        demandProcessor.processDemands(demands)
        time.sleep(30)


if __name__ == "__main__":
    main()