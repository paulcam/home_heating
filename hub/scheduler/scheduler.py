import datetime
from heating_lib import DataDAO
from conditions import Condition, TimeCondition, DayOfWeekCondition, \
    BasicTemperatureCondition, PresenceCondition
from datum import Demand
from datum import Datum
import time
from demand_processor import DemandProcessor
from snmp import SnmpPresence
from temp_manager import DataManager

# Currently abstract :(
class Schedule:
    def __init__(self, conditions, name):
        self.conditions = conditions
        self.name = name

    def __str__(self):
        return "Schedule: "+self.name

    def __repr__(self):
        return "Schedule: "+self.name

    def evaluate(self, data):
        state = True 
        for condition_tuple in self.conditions:
            modifier = condition_tuple[0]
            condition = condition_tuple[1]

            #print("Modifier: {0!s} Condition: {1!s}".format(modifier, condition))
            if modifier == "OR":
                state = Condition._or( state, condition, data )
                #print( " OR {0!s}({1!s})".format(condition, state))
            elif modifier == "AND":
                state = Condition._and( state, condition, data )
                #print(" AND {0!s}({1!s})".format(condition, state))
            else:
                print("Unknown modifier: {0!s}".format(modifier))
        if state:
            self.publishActive()

        return state

    def publishActive(self):
        key = self.name.replace(" ", "")
        datum = Datum("schedule"+key, self.name, "AS", "Active", "bool", "none", 0)
        dataDao = DataDAO()
        dataDao.publish_data(datum)


# TODO Detect and ignore stale temperatures
class MultiRoomTempSchedule(Schedule):
    def __init__(self, conditions, name, tempManager, targetType):
        super().__init__(conditions, name)
        self.tempManager = tempManager
        self.targetType = targetType
        self.expiry = 60

    def __str__(self):
        return "MultiRoomSchedule: "+self.name

    def __repr__(self):
        return "MultiRoomSchedule: "+self.name

    def evaluate(self, data):
        state = super().evaluate(data)
        if not state:
            return []
        print("{0!s} Active - checking temps {1!s}".format(self.name, self.targetType))
        active = []
        targets = self.tempManager.get_target_temps(self.targetType)

        conditions = []
        for key in targets:
            targetDatum = Datum("target{}{}".format(key.capitalize(),
                                self.targetType.capitalize()),
                                "Target {}{}".format(key.capitalize(),
                                self.targetType.capitalize()),
                                "TT", targets[key], "float",
                                "'C", 0)
            dataDao = DataDAO()
            dataDao.publish_data(targetDatum)

            condition = BasicTemperatureCondition(key, targets[key])
            conditions.append(condition)

        for condition in conditions:
            key = condition.key
            if key in targets:
                target = targets[key]
                condition.minimum = target

            if condition.evaluate(data):
                demand = Demand("heating-"+condition.key, "ON", int(self.expiry), "HEATING")
                active.append(demand)

        return active


class MultiroomPresenceSchedule(Schedule):
    def __init__(self, conditions, name,  tempManager, targetType ):
        super().__init__(conditions, name)
        self.tempManager = tempManager
        self.targetType = targetType
        self.expiry = 60

    def evaluate(self, data):
        state = super().evaluate(data)
        if not state:
            return []
        print("{0!s} Active - checking temps {1!s}".format(self.name, self.targetType))
        active = []
        targets = self.tempManager.get_target_temps(self.targetType)

        dataDao = DataDAO()

        targets = self.filterTargets( targets, data )

        conditions = []
        for key in targets:
            targetDatum = Datum("target{}{}".format(key.capitalize(),
                                self.targetType.capitalize()),
                                "Target {}{}".format(key.capitalize(),
                                self.targetType.capitalize()),
                                "TT", targets[key], "float",
                                "'C", 0)
            dataDao.publish_data(targetDatum)

            condition = BasicTemperatureCondition(key, targets[key])
            conditions.append(condition)

        for condition in conditions:
            key = condition.key
            if key in targets:
                target = targets[key]
                condition.minimum = target

            if condition.evaluate(data):
                demand = Demand("heating-"+condition.key, "ON", int(self.expiry), "HEATING")
                active.append(demand)

        return active

    def filterTargets(self, targets, data):
        activeKeys = []
        for datum in data.values():
            if not isinstance(datum, Datum):
                continue
            if not datum.key.startswith("presence"):
                continue
            if datum.value != 1:
                continue

            zone = datum.key.replace("presence", "")
            if len(zone) > 0:
                zone = zone[0].lower() + zone[1:]
                activeKeys.append(zone)
        uneffectedZones = set(targets.keys()) - set(activeKeys)
        for uneffectedZone in uneffectedZones: del targets[uneffectedZone]
        print("Presence overrides for {0!s}".format(targets))
        return targets

class Scheduler:
    def __init__(self, schedules):
        self.schedules = schedules
        self.dataDao = DataDAO()

    def get_active(self, data):
        active = []
        for schedule in self.schedules:
            active += schedule.evaluate( data )

        return active

    def update(self):
        demandProcessor = DemandProcessor()
        datums = self.dataDao.get_all_data()
        if len(datums) < 1:
          return
        if not type(datums) is dict:
          return
        now = datetime.datetime.now()
        datums["currentTime"] = now

        activeDemands = self.get_active(datums)
        self.update_presence_detection(demandProcessor, activeDemands)

        # Filter by key
        uniqueDemands = {}
        for activeDemand in activeDemands:
            uniqueDemands[activeDemand.key] = activeDemand

        for demand in uniqueDemands.values():
            demandProcessor.raiseAnyDemand(demand, demand.key)

        print("Active demands {0!s}".format(uniqueDemands))

    def update_presence_detection(self,demandProcessor, activeDemands):
        # Am I home?
        snmp_presence = SnmpPresence(ip="10.0.0.1", community="public")
        # Phone
        # 5E:43:48:75:4D:D3
        # presence = snmp_presence.isMacOnline('0x8058f81a483b')
        presence = snmp_presence.isMacOnline('0x5e4348754dd3')
        self.publish_presence(presence, "presenceHome", "presence", demandProcessor)

        presenceOffice = snmp_presence.isMacOnline('0x2cfda1bb1e78')
        self.publish_presence(presenceOffice, "presence-office", "presenceOffice", demandProcessor)

        presenceLiving = snmp_presence.isMacOnline('0x782bcb997e38')
        self.publish_presence(presenceLiving, "presence-livingRoom", "presenceLivingRoom", demandProcessor)

        presenceBedroom = snmp_presence.isMacOnline('0x1803733e37e5')
        self.publish_presence(presenceBedroom, "presence-bedroom", "presenceBedroom", demandProcessor)


        # Bedroom pc is      18 03 73 3E 37 E5
        # Livingroom PC is   78 2B CB 99 7E 38
        # Office PC is       2C FD A1 BB 1E 78

        # Am I in the living room?
        # if "groupLight1" in activeDemands:
        #     groupLightDemand = activeDemands["groupLight1"]
        #     state = 0
        #     if groupLightDemand.demandState and groupLightDemand.demandState == "ON":
        #         if not groupLightDemand.isExpired(time.time()):
        #             # Living room light is on.
        #             state = 1
        #     self.dataDao.publish_data(Datum("presence-livingroom", "Presence", "xx", state, "bool", " ", 0))

    def publish_presence(self, presence, demandKey, datumKey, demandProcessor ):
        if presence:
            demandProcessor.raiseDemand('{"'+demandKey+'":{"key":"'+demandKey+'", "demandState":"ON", "expiry":120, "category":"PRESENCE"}}')
            self.dataDao.publish_data(Datum(datumKey, datumKey, "PR", 1, "bool", " ",0))
            print("Set {0!s} to {1!s}".format(demandKey, 1))
        else:
            self.dataDao.publish_data(Datum(datumKey, datumKey, "PR", 0, "bool", " ", 0))
            print("Set {0!s} to {1!s}".format(demandKey, 0))

def main():
    time.sleep(30)
    
    all_day_start = datetime.time(hour=6, minute=30, second=0)
    all_day_end = datetime.time(hour=23, minute=00, second=0)
    all_day = TimeCondition(all_day_start, all_day_end)
    all_night = TimeCondition(all_day_end, all_day_start)

    weekend = DayOfWeekCondition([6, 7])
    weekend_routine_conditions = [("AND", all_day), ("AND",weekend)]

    weekday = DayOfWeekCondition([1,2,3,4,5])

    morning_start = datetime.time(hour=6, minute=30, second=0)
    morning_end = datetime.time(hour=8, minute=00, second=0)
    morning = TimeCondition(morning_start,morning_end)
    weekday_morning_routine_conditions = [("AND", weekday), ("AND", morning)]

    evening_start = datetime.time(hour=17, minute=00, second=0)
    evening_end = datetime.time(hour=23, minute=00, second=0)
    evening = TimeCondition(evening_start, evening_end)
    
    weekday_evening_routine_conditions = [("AND", weekday), ("AND", evening)]

    target_temps = {
        "default": {"livingRoom":10, "bedroom":10, "office":10, "hall": 10},
        "eco": {"livingRoom": 12, "bedroom": 12, "office": 16, "hall": 10},
        "preferred": {"livingRoom": 14, "bedroom": 12, "office": 18, "hall": 12},
        "boost1": {"livingRoom": 18, "bedroom": 16, "office": 18, "hall": 14},
        "comfort": {"livingRoom": 19, "bedroom": 16, "office": 20}
    }

    temp_manager = DataManager(target_temps)

    # Get thee to a service of your own!  (Also looking at you Override HTTP)
    temp_manager.start()

    maintenance_schedule = MultiRoomTempSchedule([], "Maintenance Schedule", temp_manager, "default")

    weekend_routine_schedule = MultiRoomTempSchedule(weekend_routine_conditions, "Weekend Routine", temp_manager, "eco")

    weekday_morning_routine_schedule = MultiRoomTempSchedule(weekday_morning_routine_conditions,
                                                             "Weekday Morning Routine", temp_manager, "boost1")

    weekday_evening_routine_schedule = MultiRoomTempSchedule(weekday_evening_routine_conditions,
                                                             "Weekday Evening Routine", temp_manager, "eco")

    presence_condition = PresenceCondition("presenceHome")

    presence_day_schedule = MultiRoomTempSchedule([("AND", all_day), ("AND", presence_condition)], "Presence Day",
                                                 temp_manager, "preferred")

    presence_night_schedule = MultiRoomTempSchedule([("AND", all_night), ("AND", presence_condition)], "Presence Night",
                                                   temp_manager, "eco")

    zone_presence_day_schedule = MultiroomPresenceSchedule([("AND", all_day)], "Zone Presence Day",
                                                 temp_manager, "comfort")
    zone_presence_night_schedule = MultiroomPresenceSchedule([("AND", all_night)], "Zone Presence Night",
                                                           temp_manager, "preferred")

    scheduler = Scheduler([weekend_routine_schedule,
                           weekday_morning_routine_schedule,
                           weekday_evening_routine_schedule,
                           presence_day_schedule,
                           presence_night_schedule,
                           zone_presence_day_schedule,
                           zone_presence_night_schedule,
                           maintenance_schedule])

    while True:
        time.sleep(30)
        scheduler.update()


if __name__ == "__main__":
    main()
