import tornado.web
import json
import tornado.ioloop
from threading import Thread
import datetime

from tornado.platform.asyncio import AnyThreadEventLoopPolicy, asyncio
asyncio.set_event_loop_policy(AnyThreadEventLoopPolicy())

class TempHandler(tornado.web.RequestHandler):
  def initialize(self, tempMgr):
    self.tempMgr = tempMgr

  def get(self):
    result = self.tempMgr.temps
    self.write(result)

  def post(self):
    data = json.loads(self.request.body)
    self.tempMgr.temps = data
    print("POST {}".format(self.tempMgr.temps))
    self.write(data)

class IndexHandler(tornado.web.RequestHandler):
  def initialize(self, indexFilename):
    self.indexFilename = indexFilename

  def get(self):
    index_file = open(self.indexFilename, "rb")
    data = index_file.read()
    index_file.close()
    self.write(data)


class OverrideHandler(tornado.web.RequestHandler):
  def initialize(self, tempMgr):
    self.tempMgr = tempMgr

  def set_default_headers(self):
    self.set_header("Access-Control-Allow-Origin", "*")
    self.set_header("Access-Control-Allow-Headers", "x-requested-with")
    self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

  def get(self):
    result = self.tempMgr.getActiveOverrides()
    json = {}
    for key in result.keys():
        json[key] = (
            { "key": key,
              "target": result[key]
            }
        )

    if len(json) > 0:
        self.write(json)
    else:
        self.write("[]")

  def options(self):
      self.set_status(204)
      self.finish()

  def post(self):
    override = json.loads(self.request.body.decode("utf8"))
    self.tempMgr.addOverride(override)
    print("POST {}".format(self.tempMgr.overrides))
    self.write(self.tempMgr.getActiveOverrides())


class DataManager(Thread):
    def __init__(self, temps):
        self.temps = temps
        print("Data manager initialised with {}".format(temps))
        self.overrides = {}
        super(DataManager, self).__init__()

    def get_target_temps(self, type):
        targets = {}
        if type in self.temps:
            targets = dict(self.temps[type])
        else:
            targets = dict(self.temps["default"])
        overrides = self.getActiveOverrides()
        print("Active overrides: {}".format(overrides))
        overrideKeys = overrides.keys()
        targetKeys = targets.keys()
        overriddenKeys = overrideKeys & targetKeys
        for key in overriddenKeys:
            print("Overriding temp for {} with {} overriding {}".format(key, overrides[key], targets[key]))
            targets[key] = overrides[key]
        return targets

    def getActiveOverrides(self):
        result = {}
        for key, value in self.overrides.items():
            zone = key
            target = value[0]
            expiry = value[1]
            now = datetime.datetime.now().timestamp()
            if expiry > now:
                result[zone] = target
        return result

    def addOverride(self, overrideJson):
        now = datetime.datetime.now().timestamp()
        expiry = now+overrideJson["expiry"]
        override = ( float(overrideJson["target"]), expiry )
        self.overrides[overrideJson["key"]] = override

    def run(self):
        app = tornado.web.Application([
            (r"/temps", TempHandler, dict(tempMgr=self)),
            (r"/override", OverrideHandler, dict(tempMgr=self)),
            (r"/", IndexHandler, dict(indexFilename="scheduler/index.html"))
        ])
        app.listen(8888)
        print("Running HTTP")
        tornado.ioloop.IOLoop.current().start()
