from abc import ABC, abstractmethod
import datetime
import time
from demand_processor import DemandProcessor

class Condition:
    @abstractmethod
    def evaluate(self, data):
        pass

    @classmethod
    def _or(cls, state, condition, data):
        return state or condition.evaluate(data)

    @classmethod
    def _and(cls, state, condition, data):
        return state and condition.evaluate(data)


class TimeCondition(Condition):
    def __init__(self, startTime, stopTime ):
        self.startTime = startTime
        self.stopTime = stopTime

    def __str__(self):
        return "TimeCondition"

    def evaluate(self, data):
        if "currentTime" in data:
            current_time = data['currentTime']
            #print("Found current time: {0!s}".format(str(data['currentTime'])))
            #print("Start time: {0!s} Stop time: {1!s}".format(str(self.startTime), str(self.stopTime) ) )
            if self.in_between(current_time.time(), self.startTime, self.stopTime ):
                #print("We are between those times")
                return True
            else:
                return False
        else:
            print("Current time not found in data")
            return False

    @staticmethod
    def in_between(now, start, end):
        if start <= end:
            return start <= now < end
        else: # over midnight e.g., 23:30-04:15
            return start <= now or now < end


class DayOfWeekCondition(Condition):
    # Monday is 1, Sunday is 7
    def __init__(self, active_days):
        self.active_days = active_days

    def __str__(self):
        return "DayOfWeekCondition Active Days: "+str(self.active_days)

    def evaluate(self, data):
        if "currentTime" in data:
            current_time = data['currentTime']
            #print("Found current time: {0!s}".format(str(current_time)))
            #print("Active Days: {0!s}".format(str(self.active_days)))
            if self.is_active_day(current_time.date()):
                #print("currentTime is an active day")
                return True
            else:
                return False
        else:
            print("Current time not found in data")
            return False

    def is_active_day(self, current_date):
        day_of_week = current_date.weekday()+1 # Python days are 0-6
        if day_of_week in self.active_days:
            return True
        return False


class BasicTemperatureCondition(Condition):
    def __init__(self, key, minimum):
        self.minimum = minimum
        self.key = key

    def evaluate(self, data):
        if self.key not in data:
            return False
        temperature = data[self.key].value
        currentTime = time.time()
        timestamp = data[self.key].timestamp

        # Ignore temps older than 5 minutes.
        if (currentTime - timestamp) > 300:
            return False

        if float(temperature) < -50 or float(temperature) > 100:
            print("Invalid temp reading for {} of {}".format(self.key, str(temperature)))
            return False

        print("{0!s}: {1!s} < {2!s}".format(self.key, temperature, self.minimum))
        return float(temperature) < self.minimum
    def __repr__(self):
        return "BasicTemperatureCondition( {0!s}, NIN:{1!s}".format(self.key, self.minimum)


class PresenceCondition(Condition):
    def __init__(self, key):
        self.key = key

    def evaluate(self, data):
        demand_proc = DemandProcessor()
        demands = demand_proc.get_demands()
        if self.key in demands:
            demand = demands[self.key]
        else:
            return False
        if demand_proc.demandExpired(demand):
            return False
        # there is an unexpired demand, we are true
        return True


class AnyOfCondition(Condition):
    def __init__(self, conditions):
        self.conditions = conditions

    def evaluate(self, data):
        state = False
        for condition_tuple in self.conditions:
            modifier = condition_tuple[0]
            condition = condition_tuple[1]

            #print("Modifier: {0!s} Condition: {1!s}".format(modifier, condition))
            if modifier == "OR":
                state = Condition._or( state, condition, data )
            elif modifier == "AND":
                state = Condition._and( state, condition, data )
            else:
                print("Unknown modifier: {0!s}".format(modifier))
        return state

    def __str__(self):
        message = "AnyOf "
        sep = ""
        for condition in self.conditions:
            message += "{0!s}{1!s}".format(sep,str(condition))
            sep = ","
        return message

# Tests
def main():
    test_time_condition()
    test_day_of_week_condition()


def test_time_condition():
    start = datetime.time(hour=9, minute=15, second=0)
    stop = datetime.time(hour=15, minute=30, second=0)
    the_condition = TimeCondition(start, stop)

    now = datetime.datetime(2019,1,1,0,0,0)
    data = {}

    while now.day < 2:
        data['currentTime'] = now
        status = the_condition.evaluate(data)
        print(str(now.time()) +" "+ str(status))
        now = now + datetime.timedelta(minutes=15)


def test_day_of_week_condition():
    now = datetime.datetime(2019, 1, 7, 0, 0, 0)
    data = {}
    the_condition = DayOfWeekCondition([1,3,5,7])

    while now.day < 14:
        data['currentTime'] = now
        status = the_condition.evaluate(data)
        print(str(now.strftime("%A")) + " " + str(status))
        now = now + datetime.timedelta(days=1)

if __name__ == "__main__":
        main()