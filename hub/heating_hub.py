import paho.mqtt.client as mqtt
import socketserver
import threading
import time
from threading import Timer
#from rrd_interface import RRDStorage
import json
from datum import Datum, DatumEncoder, Demand, DemandEncoder

class MQTTBridge:
    def __init__(self, ip="10.0.0.199", port=1883):
        self.client = mqtt.Client("DataHubTemp")  # create new instance
        self.client.on_disconnect=self.on_disconnect
        self.client.on_connect = self.on_connect
        self.client.connect( ip, port )
        self.client.loop_start()

        print("MQTT Sensor Bridge constructed and connecting.")

    def on_connect(self, client, userdata, flags, rc):
        print("MQTT On Connect Called with <"+str(userdata)+"> flags <"+str(flags)+"> rc <"+str(rc)+">");

    def on_disconnect(self, client, userdata, rc):
        if rc != 0:
            print("MQTT On Disconnect Called with <" + str(userdata) + "> rc <" + str(rc) + ">");

    def publish(self, datum, raw):
        topic = "/sensors/"+datum.key
        payload = raw
        print("MQTT Attempting to publish "+payload+"    ON "+topic)
        self.client.publish(topic,payload)

    def publish_direct(self, topic, payload):
        print("MQTT Attempting to publish " + payload + "    ON " + topic)
        self.client.publish(topic, payload)

mqttBridge = MQTTBridge()
testBridge = MQTTBridge(port=11883)

class DataStore:
    latest_temps = {}
    demands = {}

#rrdi = RRDStorage()

class DemandReceiverHandler(socketserver.BaseRequestHandler):
    def handle(self):
        data = self.request[0].strip()
        socket = self.request[1]
        print("Recieved demand from {}:".format(self.client_address[0]))
        data_str=data.decode("utf-8")
        print(data_str+"\n")
        datums = Demand.jsonToDict(data_str)
        # timestamp data
        for datum in datums:
            datums[datum].timestamp = time.time()
        # merge dictionaries
        DataStore.demands = {**DataStore.demands, **datums }


class DemandReceiverThread(socketserver.ThreadingMixIn, socketserver.UDPServer):
    pass

class ProbeReceiverHandler(socketserver.BaseRequestHandler):
    def handle(self):
        global mqttBridge, testBridge
        data = self.request[0].strip()
        socket = self.request[1]
        print("Recieved from {}:".format(self.client_address[0]))
        data_str=data.decode("utf-8")
        print(data_str+"\n")
        datums = Datum.jsonToDict(data_str)
        # timestamp data
        for datum in datums:
            datums[datum].timestamp = time.time()
            mqttBridge.publish(datums[datum], json.dumps(datums[datum].__dict__))
            key = str(datums[datum].key).lower()
            if datums[datum].units == "'C":
                testBridge.publish_direct("home/sensors/temperature/"+datums[datum].key, json.dumps(datums[datum].__dict__))
            elif "humidity" in key:
                testBridge.publish_direct("home/sensors/humidity/"+datums[datum].key, json.dumps(datums[datum].__dict__))
            elif "panel" in key or "battery" in key or "load" in key:
                testBridge.publish_direct("home/sensors/solar/"+datums[datum].key, json.dumps(datums[datum].__dict__))
            elif key.startswith("mains"):
                testBridge.publish_direct("home/sensors/mains/"+datums[datum].key, json.dumps(datums[datum].__dict__))
        # merge dictionaries
        DataStore.latest_temps = {**DataStore.latest_temps, **datums }


class ProbeReceiverThread(socketserver.ThreadingMixIn, socketserver.UDPServer):
    pass


class DisplaySenderHandler(socketserver.BaseRequestHandler):
    def handle(self):
        message = json.dumps(DataStore.latest_temps, cls=DatumEncoder)
        self.request.sendall(bytes(message, "utf-8"))
        print("Sent to {0!s}:{1!s}".format(self.client_address[0], message))

class DisplaySenderThread(socketserver.ThreadingMixIn, socketserver.TCPServer):
        pass

class DemandSenderHandler(socketserver.BaseRequestHandler):
    def handle(self):
        message = json.dumps(DataStore.demands, cls=DemandEncoder)
        self.request.sendall(bytes(message, "utf-8"))
        print("Sent demands to {0!s}:{1!s}".format(self.client_address[0], message))

class DemandSenderThread(socketserver.ThreadingMixIn, socketserver.TCPServer):
        pass

class UpdateTimer(Timer):
    def run(self):
        while not self.finished.is_set():
            self.finished.wait(self.interval)
            self.function(*self.args, **self.kwargs)

        self.finished.set()

def update_ha_discovery(key, value, units, datum):
    global mqttBridge
    topic = "homeassistant/sensor/{0!s}/config".format(datum.key)
    device_class = ""
    units_of_measure = units
    if datum.units.endswith("C"):
        device_class = "temperature"
        units_of_measure = "°C"
    if datum.units.endswith("W"):
        device_class = "power"
        units_of_measure = "W"

    state_topic = "/sensors/"+datum.key
    nice_name = datum.name

    device_element = ""
    if device_class != "":
        device_element = ', "device_class":"{0!s}"'.format(device_class)

    payload = '{{"name":"{0!s}" {1!s}, "state_topic":"{2!s}",' \
              '"value_template":"{{{{value_json.value}}}}", "unit_of_measurement":"{3!s}"}}'.format(nice_name, device_element, state_topic, units_of_measure)
    mqttBridge.publish_direct(topic, payload)


def update_rrd(key, value, units):
    print("Updating RRD for {0!s} with current value {1!s}".format(key, value))
    #rrdi.store_now(key, value, units)


def update_stuff():
    for key, datum in DataStore.latest_temps.items():
        value = datum.value
        key = datum.shortName
        units = datum.units
        #update_rrd(key, value, units)
        update_ha_discovery(key, value, units, datum)

if __name__ == "__main__":
    # datums = Datum.jsonToDict('{"garage": {"key":"garage", "name":"Garage", "shortName":"GR", "value":"17.5", "type":"float", "units":"*C"},'
    #                         '"bedroom": {"key":"bedroom", "name":"Bedroom", "shortName":"BR", "value":"17.5", "type":"float", "units":"*C"}}')
    #
    # for key in datums.keys():
    #     print(datums[key].name)
    #
    # jsonTxt = json.dumps(datums, cls=DatumEncoder)
    # datums = Datum.jsonToDict(jsonTxt)
    #
    # for key in datums.keys():
    #     print(datums[key].name)
    #
    #
    # exit(0)
    HOST, P_PORT, D_PORT, DEMAND_READ_PORT, DEMAND_WRITE_PORT = "0.0.0.0", 9999, 9998, 9997, 9996

    socketserver.TCPServer.allow_reuse_address = True
    socketserver.UDPServer.allow_reuse_address = True
    probe_server = ProbeReceiverThread((HOST, P_PORT), ProbeReceiverHandler)
    display_server = DisplaySenderThread((HOST, D_PORT), DisplaySenderHandler)

    demand_receiver = DemandReceiverThread((HOST, DEMAND_READ_PORT), DemandReceiverHandler)
    demand_server = DemandSenderThread((HOST, DEMAND_WRITE_PORT), DemandSenderHandler)

    try:
        th = threading.Thread(target=probe_server.serve_forever)
        th.start()

        th = threading.Thread(target=display_server.serve_forever)
        th.start()

        th = threading.Thread(target=demand_server.serve_forever)
        th.start()

        th = threading.Thread(target=demand_receiver.serve_forever)
        th.start()

        t = UpdateTimer(60.0, update_stuff)
        t.start()

        while True:
            time.sleep(50)

    except KeyboardInterrupt:        
        pass
    finally:
        probe_server.shutdown()
        probe_server.server_close()
        display_server.shutdown()
        display_server.server_close()
        demand_server.shutdown()
        demand_server.server_close()
        demand_receiver.shutdown()
        demand_receiver.server_close()
