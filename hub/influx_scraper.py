from influxdb import InfluxDBClient
from heating_lib import DataDAO, DemandDAO
import time
import datetime
from datetime import datetime
import re

datadao = DataDAO()
client = InfluxDBClient('10.0.0.199', 8086, 'root', 'root', 'test')

def camel_case_split(identifier):
    matches = re.finditer('.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)', identifier)
    return [m.group(0) for m in matches]


def process_datums():
    targets = {}
    data = datadao.get_all_data()
    for key in data:
        datum = data[key]
        timestamp = datetime.utcfromtimestamp(datum.timestamp)
        pattern = '^Panel|^Battery|^Load'
        solar = re.match(pattern, datum.name)
        pattern = '^Mains'
        mains = re.match(pattern, datum.name)
        if key.startswith("target"):
            process_target(data, key, targets)

        elif data[key].units == "'C":
            if float(datum.value) < -50 or float(datum.value) > 100:
                continue
            json_body = [
                {
                    "measurement": "temperature",
                    "tags": {
                        "key": key,
                        "name": datum.name,
                        "shortName": datum.shortName,
                        "type": datum.type,
                        "units": datum.units,
                    },
                    "time": timestamp,
                    "fields": {
                        "value": float(datum.value)
                    }
                }
            ]
        elif solar:
            json_body = [
                {
                    "measurement": "solar",
                    "tags": {
                        "key": key,
                        "name": datum.name,
                        "shortName": datum.shortName,
                        "type": datum.type,
                        "units": datum.units,
                    },
                    "time": timestamp,
                    "fields": {
                        "value": float(datum.value)
                    }
                }
            ]
        elif mains:
            json_body = [
                {
                    "measurement": "mains",
                    "tags": {
                        "key": key,
                        "name": datum.name,
                        "shortName": datum.shortName,
                        "type": datum.type,
                        "units": datum.units,
                    },
                    "time": timestamp,
                    "fields": {
                        "value": float(datum.value)
                    }
                }
            ]
        elif datum.type == "bool":
            json_body = [
                {
                    "measurement": "states",
                    "tags": {
                        "key": key,
                        "name": datum.name,
                        "shortName": datum.shortName,
                        "type": datum.type,
                        "units": datum.units,
                    },
                    "time": timestamp,
                    "fields": {
                        "value": bool(datum.value)
                    }
                }
            ]
        elif datum.type == "float":
            json_body = [
                {
                    "measurement": "other",
                    "tags": {
                        "key": key,
                        "name": datum.name,
                        "shortName": datum.shortName,
                        "type": datum.type,
                        "units": datum.units,
                    },
                    "time": timestamp,
                    "fields": {
                        "value": float(datum.value)
                    }
                }
            ]
        else:
            json_body = [
                {
                    "measurement": "other",
                    "tags": {
                        "key": key,
                        "name": datum.name,
                        "shortName": datum.shortName,
                        "type": datum.type,
                        "units": datum.units,
                    },
                    "time": timestamp,
                    "fields": {
                        "value": str(datum.value)
                    }
                }
            ]
        client.write_points(json_body)
    publish_targets(targets)

def process_target(data, key, targets):
    parts = camel_case_split(key)
    targetKey = ''.join(parts[:-1])
    if targetKey in targets:
        if targets[targetKey][0] <= data[key].value:
            targets[targetKey] = (data[key].value, parts[-1])
    else:
        targets[targetKey] = (data[key].value, parts[-1])

def publish_targets(targets):
    for targetKey in targets.keys():
        target = targets[targetKey]
        json_body = [
            {
                "measurement": "targets",
                "tags": {
                    "key": targetKey,
                    "name": targetKey.replace("target", ""),
                    "targetLabel": target[1],
                    "type": "float",
                    "units": "'C",
                },
                "time": datetime.utcnow(),
                "fields": {
                    "value": float(target[0])
                }
            }
        ]
        client.write_points(json_body)


def process_demands():
    demandDao = DemandDAO()
    demands = demandDao.get_all_data()
    demands = {key: value for (key, value) in demands.items() if not value.is_expired(time.time())}
    for demand in demands.values():
        if demand.is_expired(time.time()):
            continue
        json_body = [
            {
                "measurement": "demands",
                "tags": {
                    "key": demand.key,
                    "category": demand.category,
                    "expiry": demand.expiry,
                    "demandState": demand.demandState
                },
                "time": datetime.utcnow(),
                "fields": {
                    "value": str(demand.demandState)
                }
            }
        ]
        client.write_points(json_body)


while True:
    try:
        process_datums()
        process_demands()
        #tempManager = TempManager()
        time.sleep(60)
    except KeyboardInterrupt or InterruptedError as e:
        print(e)
        pass
    finally:
        client.close()
